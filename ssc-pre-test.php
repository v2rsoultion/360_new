<!-- header include -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Ssc Pre | Test</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="index.php" class="home-main">Home</a> <i class="fas fa-chevron-right"></i> <u class="mater"> Ssc Pre | Test</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="ssc-pre">
  <div class="container">
    <div class="row">
      <div class="col-xl-9">
        <div class="allcourse-subjects">
          <ul id="tabs" class="nav nav-tabs float-left" role="tablist">
            <li class="nav-item">
              <a id="reasoning" href="#pane-A" class="nav-link active" data-toggle="tab" role="tab">Reasoning</a>
            </li>
            <li class="nav-item">
              <a id="general-knowledge" href="#pane-B" class="nav-link" data-toggle="tab" role="tab">General Knowledge</a>
            </li>
            <li class="nav-item">
              <a id="math" href="#pane-C" class="nav-link" data-toggle="tab" role="tab">Math</a>
            </li>
            <li class="nav-item">
              <a id="tab-D" href="#pane-D" class="nav-link" data-toggle="tab" role="tab">English</a>
            </li>
          </ul>
        </div>
        <h6 class="float-right">Total Time : <span>00:30:00</span></h6>
        <div class="clearfix"></div>
        <div id="content" class="tab-content" role="tablist">
          <div id="pane-A" class="card tab-pane fade show active" role="tabpanel" aria-labelledby="reasoning">
            <!-- Note: New place of `data-parent` -->
            <div class="" data-parent="#content" role="tabpanel" id="pane-A" class="card tab-pane fade show active" aria-labelledby="reasoning">
              <form>
                <div class="card-body">
                  <div class="col-xl-12">
                    <i class="float-left">Q.1</i>
                    <h3 class="float-left"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h3>
                    <div class="clearfix"></div>
                    <div class="row question-in">
                      <div class="col-xl-6">
                        <div class="options-ques">
                          <div class="capital-word text-uppercase float-left">A</div>
                          <div class="answer-options text-capitalize float-left">Very Late</div>
                          <div class="radio">
                            <input id="option-1" name="radio" type="radio">
                            <label  for="option-1" class="radio-label "></label>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-xl-6">
                        <div class="options-ques">
                          <div class="capital-word text-uppercase float-left">B</div>
                          <div class="answer-options text-capitalize float-left">After Sunrise</div>
                          <div class="radio">
                            <input id="option-2" name="radio" type="radio">
                            <label  for="option-2" class="radio-label"></label>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-xl-6">
                        <div class="options-ques">
                          <div class="capital-word text-uppercase float-left">C</div>
                          <div class="answer-options text-capitalize float-left">Very Early</div>
                          <div class="radio">
                            <input id="option-3" name="radio" type="radio">
                            <label  for="option-3" class="radio-label"></label>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-xl-6">
                        <div class="options-ques">
                          <div class="capital-word text-uppercase float-left">D</div>
                          <div class="answer-options text-capitalize float-left">At Midnight</div>
                          <div class="radio">
                            <input id="option-4" name="radio" type="radio">
                            <label  for="option-4" class="radio-label"></label>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                    <div class="answer-buttons">
                      <button type="button" title="PREVIOUS" class="btn btn-primary all-button text-uppercase float-left">< Previous</button>
                      <div class="tag-erase text-center">
                        <button type="button" title="TAG" class="btn btn-primary all-button text-uppercase"> Tag</button>
                        <button type="button" title="ERASE" class="btn btn-primary all-button text-uppercase">erase</button>
                      </div>
                      <button type="button" title="NEXT" class="btn btn-primary all-button text-uppercase float-right">Next ></button>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
            </div>
          </div>
          <div id="pane-B" class="card tab-pane fade" role="tabpanel" aria-labelledby="general-knowledge">
          <div class="" data-parent="#content" role="tabpanel" aria-labelledby="general-knowledge">
          <div class="card-body">
          <div class="col-xl-12">
          <i class="float-left">Q.2</i>
          <h3 class="float-left"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h3>
          <div class="clearfix"></div>
          <div class="row question-in">
          <div class="col-xl-6">
          <div class="options-ques">
          <div class="capital-word text-uppercase float-left">A</div>
          <div class="answer-options text-capitalize float-left">Very Late</div>
          <div class="clearfix"></div>
          </div>
          </div>
          <div class="col-xl-6">
          <div class="options-ques">
          <div class="capital-word text-uppercase float-left">B</div>
          <div class="answer-options text-capitalize float-left">After Sunrise</div>
          <div class="clearfix"></div>
          </div>
          </div>
          <div class="col-xl-6">
          <div class="options-ques">
          <div class="capital-word text-uppercase float-left">C</div>
          <div class="answer-options text-capitalize float-left">Very Early</div>
          <div class="clearfix"></div>
          </div>
          </div>
          <div class="col-xl-6">
          <div class="options-ques">
          <div class="capital-word text-uppercase float-left">D</div>
          <div class="answer-options text-capitalize float-left">At Midnight</div>
          <div class="clearfix"></div>
          </div>
          </div>
          </div>
          <div class="answer-buttons">
          <button type="button" title="PREVIOUS" class="btn btn-primary all-button text-uppercase float-left">< Previous</button>
          <div class="tag-erase text-center">
          <button type="button" title="TAG" class="btn btn-primary all-button text-uppercase"> Tag</button>
          <button type="button" title="ERASE" class="btn btn-primary all-button text-uppercase">erase</button>
          </div>
          <button type="button" title="NEXT" class="btn btn-primary all-button text-uppercase float-right">Next ></button>
          <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          </div>
          </div>
          </div>
          </div>
          <div id="pane-C" class="card tab-pane fade" role="tabpanel" aria-labelledby="math">
          <div class="" data-parent="#content" role="tabpanel" aria-labelledby="math">
          <div class="card-body">
          <div class="col-xl-12">
          <i class="float-left">Q.3</i>
          <h3 class="float-left"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h3>
          <div class="clearfix"></div>
          <div class="row question-in">
          <div class="col-xl-6">
          <div class="options-ques">
          <div class="capital-word text-uppercase float-left">A</div>
          <div class="answer-options text-capitalize float-left">Very Late</div>
          <div class="clearfix"></div>
          </div>
          </div>
          <div class="col-xl-6">
          <div class="options-ques">
          <div class="capital-word text-uppercase float-left">B</div>
          <div class="answer-options text-capitalize float-left">After Sunrise</div>
          <div class="clearfix"></div>
          </div>
          </div>
          <div class="col-xl-6">
          <div class="options-ques">
          <div class="capital-word text-uppercase float-left">C</div>
          <div class="answer-options text-capitalize float-left">Very Early</div>
          <div class="clearfix"></div>
          </div>
          </div>
          <div class="col-xl-6">
          <div class="options-ques">
          <div class="capital-word text-uppercase float-left">D</div>
          <div class="answer-options text-capitalize float-left">At Midnight</div>
          <div class="clearfix"></div>
          </div>
          </div>
          </div>
          <div class="answer-buttons">
          <button type="button" title="PREVIOUS" class="btn btn-primary all-button text-uppercase float-left">< Previous</button>
          <div class="tag-erase text-center">
          <button type="button" title="TAG" class="btn btn-primary all-button text-uppercase"> Tag</button>
          <button type="button" title="ERASE" class="btn btn-primary all-button text-uppercase">erase</button>
          </div>
          <button type="button" title="NEXT" class="btn btn-primary all-button text-uppercase float-right">Next ></button>
          <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          </div>
          </div>
          </div>
          </div>
        </div>
        <div class="preview text-center">
        <button type="button" title="PREVIEW SUBMIT" class="btn btn-primary all-button text-uppercase">Preview Submit</button>
        </div>
        </form>
      </div>
      <div class="col-xl-3">
        <div class="view-quiz">
          <h5 class="text-center text-capitalize">You Are Views Quiz Section</h5>
          <ul>
            <li class="right-answer"><a href="">1</a></li>
            <li class="right-answer"><a href="">2</a></li>
            <li class="right-answer"><a href="">3</a></li>
            <li class="wrong-answer"><a href="">4</a></li>
            <li class="tempted"><a href="">5</a></li>
            <li class="right-answer"><a href="">6</a></li>
            <li class="right-answer"><a href="">7</a></li>
            <li class="tempted"><a href="">8</a></li>
            <li><a href="">9</a></li>
            <li><a href="">10</a></li>
            <li><a href="">11</a></li>
            <li><a href="">12</a></li>
            <li><a href="">13</a></li>
            <li><a href="">14</a></li>
            <li><a href="">15</a></li>
            <li><a href="">16</a></li>
            <li><a href="">17</a></li>
            <li><a href="">18</a></li>
            <li><a href="">19</a></li>
            <li><a href="">20</a></li>
            <li><a href="">21</a></li>
            <li><a href="">22</a></li>
            <li><a href="">23</a></li>
            <li><a href="">24</a></li>
            <li><a href="">25</a></li>
            <li><a href="">26</a></li>
            <li><a href="">27</a></li>
            <li><a href="">28</a></li>
            <li><a href="">29</a></li>
            <li><a href="">30</a></li>
            <li><a href="">31</a></li>
            <li><a href="">32</a></li>
            <li><a href="">33</a></li>
            <li><a href="">34</a></li>
            <li><a href="">35</a></li>
          </ul>
        </div>
        <div class="legends">
          <h5 class="text-capitalize text-center">Legends</h5>
          <button class="all-button text-uppercase unattemp" title="UnAttempted">UnAttempted</button>
          <button class="all-button text-uppercase attemp" title="Attempted">Attempted</button>
          <button class="all-button text-uppercase tagged" title="Tagged">Tagged </button>
          <button class="all-button text-uppercase ttem" title="Ttempted & Tagged ">ttempted & Tagged </button>
        </div>
      </div>
    </div>
  </div>
</section>
<!--     footer include --> 
<?php
  include 'footer.php';
  ?>