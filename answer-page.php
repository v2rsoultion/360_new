<!-- header include -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Question & answer</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="index.php" class="home-main">Home</a>  >  <u class="mater">Question & answer</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="ques-ans" class="asks-ans">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xl-9 col-12">
        <div class="question-type">
          <div class="col-xl-12 col-md-12 col-12">
            <div class="row">
              <div class="col-xl-8 col-md-8 col-12">
                <u class="float-left">Q. 1 </u>
                <h3 class="float-left">Lorem Ipsum is simply dummy text of the printing? </h3>
                <div class="clearfix"></div>
                <ul>
                  <li><a href="" title="Lorem Ipsum">Lorem Ipsum</a> </li>
                  <li><a href="" title="Lorem Set">Lorem Set</a>  </li>
                  <li><a href="" title="Lorem Ipsum">Lorem Ipsum</a></li>
                  <li><a href="" title="Lorem Set">Lorem Set</a></li>
                </ul>
                <div class="ques-side">
                  <a href=""> <img src="images/boy.png" class="float-left" alt="boy"></a>
                  <div class="ques-side2 float-left">
                    <span class="text-center"> <a href="" title="Brijpal Singh"><u>Brijpal Singh</u></a>
                    <br><i class="fas fa-calendar-alt"></i>Oct 10,2015 
                    </span>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-xl-4 col-md-4 col-12 side-men">
                <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                  <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                  <a class="dropdown-item" href="#" title="Task">Task</a>
                  <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                </div>
                <div class="clearfix"></div>
                <div class="like-dislike float-right">
                  <a title="Answer" data-toggle="modal" data-target="#ask-answer"><span><i class="fas fa-comment-alt"></i>Answer</span></a>
                  <span><i class="fas fa-eye"></i>300</span>
                  <span class="like"><i class="fas fa-thumbs-up"></i>31k</span>
                  <span class="t-down"><i class="fas fa-thumbs-down"></i>31k</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row ask-ques">
          <div class="col-xl-6 col-6 col-md-6">
            <h5>Lorem ipsum dolor sit amet, consectetur</h5>
          </div>
          <div class="col-xl-6 col-6 col-md-6">
            <div class="post-ques float-right">
              <button title="Post Your Answer"  data-toggle="modal" data-target="#ask-answer" class="text-capitalize"  data-backdrop="static" data-keyboard="false">Post Your Answer</button>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="answer-pannel">
          <div class="question-type answer-type">
            <div class="col-xl-12 col-md-12 col-12 answer-pading">
              <div class="row">
                <div class="col-xl-9 col-md-8 col-12">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div class="col-xl-3 col-md-4 col-12 side-men">
                  <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                    <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                    <a class="dropdown-item" href="#" title="Task">Task</a>
                    <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-9 col-md-8 col-12">
                  <div class="ques-side">
                    <a href=""><img src="images/boy.png" class="float-left" alt="boy"></a>
                    <div class="ques-side2 float-left">
                      <span class="text-center student-name"><a href="" title="Brijpal Singh"><u>Brijpal Singh</u></a>
                      <i class="fas fa-calendar-alt"></i>Oct 10,2015 
                      </span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                  <div class="like-dislike float-right">
                    <span class=""><i class="fas fa-thumbs-up"></i>542k</span>
                    <span class="t-down"><i class="fas fa-thumbs-down"></i>542k</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
   
           <div class="question-type answer-type">
            <div class="col-xl-12 col-md-12 col-12 answer-pading">
              <div class="row">
                <div class="col-xl-9 col-md-8 col-12">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div class="col-xl-3 col-md-4 col-12 side-men">
                  <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                    <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                    <a class="dropdown-item" href="#" title="Task">Task</a>
                    <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-9 col-md-8 col-12">
                  <div class="ques-side">
                    <a href=""><img src="images/boy.png" class="float-left" alt="boy"></a>
                    <div class="ques-side2 float-left">
                      <span class="text-center student-name"><a href="" title="Brijpal Singh"><u>Brijpal Singh</u></a>
                      <i class="fas fa-calendar-alt"></i>Oct 10,2015 
                      </span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                  <div class="like-dislike float-right">
                    <span class=""><i class="fas fa-thumbs-up"></i>542k</span>
                    <span class="t-down"><i class="fas fa-thumbs-down"></i>542k</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
           <div class="question-type answer-type">
            <div class="col-xl-12 col-md-12 col-12 answer-pading">
              <div class="row">
                <div class="col-xl-9 col-md-8 col-12">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div class="col-xl-3 col-md-4 col-12 side-men">
                  <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                    <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                    <a class="dropdown-item" href="#" title="Task">Task</a>
                    <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-9 col-md-8 col-12">
                  <div class="ques-side">
                    <a href=""><img src="images/boy.png" class="float-left" alt="boy"></a>
                    <div class="ques-side2 float-left">
                      <span class="text-center student-name"><a href="" title="Brijpal Singh"><u>Brijpal Singh</u></a>
                      <i class="fas fa-calendar-alt"></i>Oct 10,2015 
                      </span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                  <div class="like-dislike float-right">
                    <span class=""><i class="fas fa-thumbs-up"></i>542k</span>
                    <span class="t-down"><i class="fas fa-thumbs-down"></i>542k</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
             <div class="question-type answer-type">
            <div class="col-xl-12 col-md-12 col-12 answer-pading">
              <div class="row">
                <div class="col-xl-9 col-md-8 col-12">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div class="col-xl-3 col-md-4 col-12 side-men">
                  <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                    <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                    <a class="dropdown-item" href="#" title="Task">Task</a>
                    <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-9 col-md-8 col-12">
                  <div class="ques-side">
                    <a href=""><img src="images/boy.png" class="float-left" alt="boy"></a>
                    <div class="ques-side2 float-left">
                      <span class="text-center student-name"><a href="" title="Brijpal Singh"><u>Brijpal Singh</u></a>
                      <i class="fas fa-calendar-alt"></i>Oct 10,2015 
                      </span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                  <div class="like-dislike float-right">
                    <span class=""><i class="fas fa-thumbs-up"></i>542k</span>
                    <span class="t-down"><i class="fas fa-thumbs-down"></i>542k</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="question-type answer-type">
            <div class="col-xl-12 col-md-12 col-12 answer-pading">
              <div class="row">
                <div class="col-xl-9 col-md-8 col-12">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div class="col-xl-3 col-md-4 col-12 side-men">
                  <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                    <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                    <a class="dropdown-item" href="#" title="Task">Task</a>
                    <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-9 col-md-8 col-12">
                  <div class="ques-side">
                    <a href=""><img src="images/boy.png" class="float-left" alt="boy"></a>
                    <div class="ques-side2 float-left">
                      <span class="text-center student-name"><a href="" title="Brijpal Singh"><u>Brijpal Singh</u></a>
                      <i class="fas fa-calendar-alt"></i>Oct 10,2015 
                      </span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                  <div class="like-dislike float-right">
                    <span class=""><i class="fas fa-thumbs-up"></i>542k</span>
                    <span class="t-down"><i class="fas fa-thumbs-down"></i>542k</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="question-type answer-type">
            <div class="col-xl-12 col-md-12 col-12 answer-pading">
              <div class="row">
                <div class="col-xl-9 col-md-8 col-12">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div class="col-xl-3 col-md-4 col-12 side-men">
                  <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                    <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                    <a class="dropdown-item" href="#" title="Task">Task</a>
                    <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-9 col-md-8 col-12">
                  <div class="ques-side">
                    <a href=""><img src="images/boy.png" class="float-left" alt="boy"></a>
                    <div class="ques-side2 float-left">
                      <span class="text-center student-name"><a href="" title="Brijpal Singh"><u>Brijpal Singh</u></a>
                      <i class="fas fa-calendar-alt"></i>Oct 10,2015 
                      </span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                  <div class="like-dislike float-right">
                    <span class=""><i class="fas fa-thumbs-up"></i>542k</span>
                    <span class="t-down"><i class="fas fa-thumbs-down"></i>542k</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <div class="question-type answer-type">
            <div class="col-xl-12 col-md-12 col-12 answer-pading">
              <div class="row">
                <div class="col-xl-9 col-md-8 col-12">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div class="col-xl-3 col-md-4 col-12 side-men">
                  <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                    <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                    <a class="dropdown-item" href="#" title="Task">Task</a>
                    <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-9 col-md-8 col-12">
                  <div class="ques-side">
                    <a href=""><img src="images/boy.png" class="float-left" alt="boy"></a>
                    <div class="ques-side2 float-left">
                      <span class="text-center student-name"><a href="" title="Brijpal Singh"><u>Brijpal Singh</u></a>
                      <i class="fas fa-calendar-alt"></i>Oct 10,2015 
                      </span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                  <div class="like-dislike float-right">
                    <span class=""><i class="fas fa-thumbs-up"></i>542k</span>
                    <span class="t-down"><i class="fas fa-thumbs-down"></i>542k</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-4 d-none d-md-block">
      <div class="ques-sidebar">
        <h4>Unanswered Questions</h4>
        <ul>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <a href="" class="all-button" title="View All">View All</a>
        </ul>
      </div>
      <div class="ques-sidebar">
        <h4>Related Questions</h4>
        <ul>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?">Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <li>
            <a href="" title="Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?" >Lorem ipsum dolor sit amet, cons-ctetur adipiscing elit?</a>
            <div class="clearfix"></div>
            <span>asked on on 31st Jan, 2019</span>
          </li>
          <a href="" class="all-button" title="View All">View All</a>
        </ul>
      </div>
      <a href="" title="Add"> <img src="images/add.png" class="img-fluid adds-img" alt="add"></a>
    </div>
  </div>
  </div>
</section>
<div class="modal ask-modal centered-modal in" id="ask-answer">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Ask Answer</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <h5> Q. 1  Lorem Ipsum is simply dummy text of the printing? </h5>
        <div class="form-group">
          <textarea class="form-control" placeholder="Type something here......" rows="5" id="comment"></textarea>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn all-button" title="SAVE">SAVE</button>
        <button type="button" class="btn cancel-button" data-dismiss="modal" title="CANCEL">CANCEL</button>
      </div>
    </div>
  </div>
</div>
<!--     footer include --> 
<?php
  include 'footer.php';
  ?>