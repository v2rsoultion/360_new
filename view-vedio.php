<!-- header include  -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-5">
        <h4 class="text-capitalize">What Is SEO</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-7">
        <span class="text-capitalize float-right"><a href="index.php" class="home-main">Home</a> <i class="fas fa-chevron-right"></i> <a href="videos-tutorials.php" class="home-main">Videos</a> <i class="fas fa-chevron-right"></i>  <u class="mater">What Is SEO</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="view-vedio">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-xl-9 col-12">
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/3UMBcd3TyhQ?list=PLjVLYmrlmjGcCeELcp2VU66XHlmyoPRpM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" class="my_class_name"></iframe>
        <div id="video-des">
          <h5>What Is SEO / Search Engine Optimization? </h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto be vitae dicta sunt explicabo. </p>
          <p class="quotes-des"><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor inc ididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</i></p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <p>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto be vitae dicta sunt explicabo.
          </p>
        </div>
     
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        <?php 
          include 'sidebar-right.php';
          ?>
      </div>
         </div>
  </div>
</section>
<section>
  <div class="container">
       <div class="related-vedios">
        <h2 class="text-uppercase text-center">Related Videos</h2>
          <div class="col-xl-10 offset-xl-1">
               <div class="owl-carousel owl-theme owl-loaded" id="related-videos">
               <div class="item">
            <div class="related-1">
              <iframe width="100%" height="100%" src="https://www.youtube.com/embed/3UMBcd3TyhQ?list=PLjVLYmrlmjGcCeELcp2VU66XHlmyoPRpM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" class="my_class_name"></iframe>
              <a href="" title="Lorem ipsum dolor sit amet consectetur elit."><h5>Lorem ipsum dolor sit amet consectetur elit.</h5></a>
          </div>
          </div>
            <div class="item">           
            <div class="related-1">
              <iframe width="100%" height="100%" src="https://www.youtube.com/embed/3UMBcd3TyhQ?list=PLjVLYmrlmjGcCeELcp2VU66XHlmyoPRpM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" class="my_class_name"></iframe>
              <a href="" title="Lorem ipsum dolor sit amet consectetur elit."><h5>Lorem ipsum dolor sit amet consectetur elit.</h5></a>
            </div>
          </div>
                 <div class="item">
            <div class="related-1">
              <iframe width="100%" height="100%" src="https://www.youtube.com/embed/3UMBcd3TyhQ?list=PLjVLYmrlmjGcCeELcp2VU66XHlmyoPRpM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" class="my_class_name"></iframe>
              <a href="" title="Lorem ipsum dolor sit amet consectetur elit."><h5>Lorem ipsum dolor sit amet consectetur elit.</h5></a>
            </div>
          </div>
           <div class="item">
            <div class="related-1">
              <iframe width="100%" height="100%" src="https://www.youtube.com/embed/3UMBcd3TyhQ?list=PLjVLYmrlmjGcCeELcp2VU66XHlmyoPRpM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" class="my_class_name"></iframe>
              <a href="" title="Lorem ipsum dolor sit amet consectetur elit."><h5>Lorem ipsum dolor sit amet consectetur elit.</h5></a>
          </div>
          </div>
        </div>
        </div>
      </div>
    </div>
 </section>
<!--     footer include --> 
<?php
  include 'footer.php';
  ?>
<!--               <iframe width="100%" height="100%" src="https://www.youtube.com/embed/3UMBcd3TyhQ?list=PLjVLYmrlmjGcCeELcp2VU66XHlmyoPRpM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" class="my_class_name"></iframe>