<aside>
  <div class="left-side-panel">
    <h2 class="text-left">Digital Marketing Introduction</h2>
    <ul>
      <li><a href="" title="CSS Online Training - Home"><i class="fas fa-chevron-right"></i>CSS Online Training - Home</a></li>
      <li><a href="" title="CSS - Introduction"><i class="fas fa-chevron-right"></i>CSS - Introduction</a></li>
      <li><a href="" title="CSS - Basic Syntax"><i class="fas fa-chevron-right"></i>CSS - Basic Syntax</a></li>
      <li><a href="" title="CSS - Selectors"><i class="fas fa-chevron-right"></i>CSS - Selectors</a></li>
      <li><a href="" title="CSS - Inclusion in HTML"><i class="fas fa-chevron-right"></i>CSS - Inclusion in HTML</a></li>
      <li><a href="" title="CSS - Colors"><i class="fas fa-chevron-right"></i>CSS - Colors</a></li>
      <li><a href="" title="CSS - Backgrounds"><i class="fas fa-chevron-right"></i>CSS - Backgrounds</a></li>
      <li><a href="" title="CSS -Font"><i class="fas fa-chevron-right"></i>CSS -Font</a></li>
      <li><a href="" title="CSS - Text"><i class="fas fa-chevron-right"></i>CSS - Text</a></li>
      <li><a href="" title="CSS - Images"><i class="fas fa-chevron-right"></i>CSS - Images</a></li>
      <li><a href="" title="CSS - Links"><i class="fas fa-chevron-right"></i>CSS - Links</a></li>
    </ul>
  </div>
  <div class="left-side-panel">
    <h2 class=" text-left">Digital Marketing Algorithm</h2>
      <ul>
      <li><a href="" title="CSS Online Training - Home"><i class="fas fa-chevron-right"></i>CSS Online Training - Home</a></li>
      <li><a href="" title="CSS - Introduction"><i class="fas fa-chevron-right"></i>CSS - Introduction</a></li>
      <li><a href="" title="CSS - Basic Syntax"><i class="fas fa-chevron-right"></i>CSS - Basic Syntax</a></li>
      <li><a href="" title="CSS - Selectors"><i class="fas fa-chevron-right"></i>CSS - Selectors</a></li>
      <li><a href="" title="CSS - Inclusion in HTML"><i class="fas fa-chevron-right"></i>CSS - Inclusion in HTML</a></li>
      <li><a href="" title="CSS - Colors"><i class="fas fa-chevron-right"></i>CSS - Colors</a></li>
      <li><a href="" title="CSS - Backgrounds"><i class="fas fa-chevron-right"></i>CSS - Backgrounds</a></li>
      <li><a href="" title="CSS -Font"><i class="fas fa-chevron-right"></i>CSS -Font</a></li>
      <li><a href="" title="CSS - Text"><i class="fas fa-chevron-right"></i>CSS - Text</a></li>
      <li><a href="" title="CSS - Images"><i class="fas fa-chevron-right"></i>CSS - Images</a></li>
      <li><a href="" title="CSS - Links"><i class="fas fa-chevron-right"></i>CSS - Links</a></li>
    </ul>
  </div>
</aside>