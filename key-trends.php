<!-- header include -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Key Trends & Concepts</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="index.php" class="home-main">Home</a>  <i class="fas fa-chevron-right"></i>  <u class="mater">Key Trends & Concepts</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="key-trends">
  <div class="container">
    <div class="row">
      <!-- for responsive -->
      <div class="col-xl-9 col-12 col-md-9">
        <div id="key-form">
          <form name="myform" method="GET" action="">
            <div class="input-group" id="seacr_panel">
              <input type="text" name="s" class="form-control vedio-search" value="" placeholder="Search tag here">
              <span class="input-group-btn">
              <button type="submit" title="Search" class="btn btn-default header-submit">SEARCH</button>
              </span>
            </div>
          </form>
          <div class="all-button">
            <ul>
              <li><a href="" class="text-uppercase active" title="All">All</a></li>
              <li><a href="" class="text-uppercase" title="A">A</a></li>
              <li><a href="" class="text-uppercase" title="B">B</a></li>
              <li><a href="" class="text-uppercase" title="C">C</a></li>
              <li><a href="" class="text-uppercase" title="D">D</a></li>
              <li><a href="" class="text-uppercase" title="E">E</a></li>
              <li><a href="" class="text-uppercase" title="F">F</a></li>
              <li><a href="" class="text-uppercase" title="G">G</a></li>
              <li><a href="" class="text-uppercase" title="H">H</a></li>
            </ul>
          </div>
        </div>


        <?php for ($i=0; $i < 10 ; $i++) {  ?>
         
        <div class="col-xl-12 padding-remove">
          <div class="answer-panel">
            <div class="a-panel float-left">
              <div class="panel-in text-center text-uppercase">
                A
              </div>
            </div>
            <div class="answer-dis float-left">
              <h5>Lorem Ipsum is simply dummy text of the printing? </h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="col-xl-12 padding-remove">
          <div class="answer-panel">
            <div class="a-panel float-left">
              <div class="panel-in text-center text-uppercase">
                B
              </div>
            </div>
            <div class="answer-dis float-left">
              <h5>Lorem Ipsum is simply dummy text of the printing? </h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
     
        <?php   } ?>
        
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        <?php 
          include 'sidebar-right.php';
          ?>
      </div>
    </div>
  </div>
</section>
<!--     footer include --> 
<?php
  include 'footer.php';
  ?>