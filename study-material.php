<!-- header include -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-12 col-md-12 col-12">
        <span class="text-uppercase"><a href="index.php" class="home-main">Home</a>  >  <a href="" class="mater">Study Material</a></span>
      </div>
    </div>
  </div>
</section>
<section id="digital-marketing">
  <div class="container">
    <div class="row">
      <div class="sidemenu"> Side Bar
        <button class="toggle float-right"><i class="fas fa-align-justify"></i></button>
      </div>
      <div class="col-xl-3 col-12 col-md-3" id="target">
        <!-- include left side bar -->
        <?php include 'sidebar-left.php';
          ?>
      </div>
      <!-- for responsive -->
      <div class="col-xl-7 col-12 col-md-7">
        <div class="mid-section">
          <div class="digital-head">
            <h1 class="text-uppercase float-left">Digital Marketing</h1>
          </div>
          <div class="digital-head-img"> <img src="images/sidedigital-img.png" alt="sidedigital-img" class="float-right img-fluid"></div>
          <div class="clearfix"></div>
        </div>
        <div class="digital-intro">
          <div class="row">
            <div class="col-xl-2 col-3 col-md-2">
              <a href="" title="Previous"> <button type="button" class="btn btn-primary float-left">< Previous</button></a>
            </div>
            <div class="col-xl-8 col-6  col-md-8">
              <h5 class="text-capitalize text-center">digital Marketing Introduction</h5>
            </div>
            <div class="col-xl-2 col-3 col-md-2">
              <a href="" title="Next"> <button type="button" class="btn btn-primary float-right"> Next ></button></a>
            </div>
            <div class="clearfix"></div>
          </div>
          <p>Today’s time of Internet has opened the gateway of tremendous digital marketing opportunities for businesses. By utilizing different channels of digital marketing,
            businesses cannot just share their product and services online; additionally they can gain clients for their business, entice them and can convert them to boost their ROI. The speed and straight-forwardness with which the digital media transmits data and support a business is astonishing.  In this Introduction to Digital Marketing E-Guide, every single aspect of Digital Marketing will be discussed to help marketers
            understand what Digital Marketing is, how it functions, and how it can help them
            optimize their marketing campaign.
          </p>
          <p>The world is super-connected nowadays and all things considered, marketing and advertising are no more the same as they once were. This is particularly valid because of the ascent of online networking, which has changed how organizations speak with potential and existing customers.</p>
          <p>So, before jump onto the introduction to Digital Marketing, let’s know what precisely Digital Marketing is and what does it incorporate? Essentially, it is an aggregate term, which is utilized where advertising and marketing meet web innovation and different types of online media platforms. Let’s firstly throw some light on the basics of Digital Marketing via the definition given below, this is the first step when we talk about the introduction to Digital Marketing.</p>
          <div class=" prev-next">
            <a href="" title="Previous"> <button type="button" class="btn btn-primary float-left">< Previous</button></a>
            <a href="" title="Next"> <button type="button" class="btn btn-primary next-bt float-right"> Next ></button></a>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-xl-2 col-12 col-md-2 adds d-none d-md-block">
        <?php 
        include 'sidebar-right.php';
        ?>
      </div>
    </div>
  </div>
</section>
<!--     footer include --> 
<?php
  include 'footer.php';
  ?>