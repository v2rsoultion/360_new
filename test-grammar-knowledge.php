<!-- header include -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">All Competition Exams</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="index.php" class="home-main">Home</a>  <i class="fas fa-chevron-right"></i>  <u class="mater">  Topics</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="all-competition" class="all-test">
  <div class="container">
    <div class="row">
      <div class="sidemenu"> Side Bar
        <button class="toggle float-right"><i class="fas fa-align-justify"></i></button>
      </div>
      <div class="col-xl-3 col-12 col-md-4" id="target">
        <div class="left-side-panel">
          <h2 class=" text-left">Test Your Grammar
            Knowledge
          </h2>
          <ul>
            <li><a href="" title="Tenses"><i class="fas fa-chevron-right"></i>Tenses</a></li>
            <li><a href="" title="Modal Verbs"><i class="fas fa-chevron-right"></i>Modal Verbs</a></li>
            <li><a href="" title="Prepositions"><i class="fas fa-chevron-right"></i>Prepositions</a></li>
            <li><a href="" title="SSC Quick Skill Online Test"><i class="fas fa-chevron-right"></i>SSC Quick Skill Online Test</a></li>
            <li><a href="" title="Verbs"><i class="fas fa-chevron-right"></i>Verbs</a></li>
            <li><a href="" title="If Conditions"><i class="fas fa-chevron-right"></i>If Conditions </a></li>
            <li><a href="" title="Articles"><i class="fas fa-chevron-right"></i>Articles</a></li>
            <li><a href="" title="Active & Passive Voice"><i class="fas fa-chevron-right"></i>Active & Passive Voice</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xl-9 col-md-8">
        <div class="all-exams">
          <h3>Test Your Grammar Knowledge</h3>
          <p>In this section of the website you can check your grammar knowledge and level by giving online exams. You will find exams for all the users as per their levels <b>(Beginner, Intermediate and Advanced).</b> You will get your marks with the correct answers after submitting the exams. </p>
          <div class="row">
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="Tenses">
                <div class="grammar-test">
                  <div class="show-text text-center float-left">T</div>
                  <h5 class="float-left">Tenses</h5>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="Modal Verbs">
                <div class="grammar-test">
                  <div class="show-text text-center modal-verb float-left">M</div>
                  <h5 class="float-left">Modal Verbs</h5>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="SSC (CGL-CPO)">
                <div class="grammar-test online">
                  <div class="show-text text-center online-tests float-left">S</div>
                  <h5 class="float-left">
                    SSC (CGL-CPO)
                    <p>Quick online test</p>
                  </h5>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="Prepositions">
                <div class="grammar-test">
                  <div class="show-text text-center prepositions float-left">P</div>
                  <h5 class="float-left">Prepositions</h5>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="If Conditions">
                <div class="grammar-test">
                  <div class="show-text text-center if-condition float-left">I</div>
                  <h5 class="float-left">If Conditions</h5>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="Articles">
                <div class="grammar-test">
                  <div class="show-text text-center artcls float-left">A</div>
                  <h5 class="float-left">Articles</h5>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="Verbs">
                <div class="grammar-test">
                  <div class="show-text text-center verb float-left">V</div>
                  <h5 class="float-left">Verbs</h5>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="Active & Passive Voice">
                <div class="grammar-test actives">
                  <div class="show-text text-center active-voice float-left">T</div>
                  <h5 class="float-left">Active &<br>
                    Passive Voice
                  </h5>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
            <div class="col-xl-12">
              <h4>What Does English App Provide You Here?</h4>
              <p>English App is a completely free online platform to learn and test your grammar knowledge and also test your preparation for your upcoming competition exam. On the website you will find complete grammar topics (coming soon) which we have explained in an easy and simple way. After learning grammar you can check your grammar knowledge by giving the online exams free of cost.</p>
              <p>
                We have divided the exams in three different parts to make sure our users get what they are looking for. Let me tell you one more important point if you are preparing for one of these competition exams <b>(SSC CGL, CHSL (10+2), CPO, BANK P.O, CLERK, LDC, NDA, CDS and Air Force Airman X & Y Groups)</b> then you are on the right website because here you will get all previous years question papers with complete solution. We have many practice and mock sets as per the SSC CGL new pattern. As we all know SSC has already made all SSC exams online so at English Aliens you will find the same way to practise your skills. Our model question papers are based on the same pattern as SSC has for TIER I and TIER II. So our students should understand the complete process and they can score much.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!--     footer include --> 
<?php
  include 'footer.php';
  ?>