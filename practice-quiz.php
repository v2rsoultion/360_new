<!-- header include -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Practice Quiz Test</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="index.php" class="home-main">Home</a>  <i class="fas fa-chevron-right"></i>  <u class="mater">Practice Quiz Test</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="practice-quiz">
  <div class="container">
    <div class="row">
      <div class="sidemenu"> Side Bar
        <button class="toggle float-right"><i class="fas fa-align-justify"></i></button>
      </div>
      <div class="col-xl-3 col-12 col-md-4" id="target">
        <!-- include left side bar -->
        <?php include 'sidebar-left.php';
          ?>
      </div>
      <!-- for responsive -->
      <div class="col-xl-6 col-12 col-md-8">
        <h3>Practice Quiz Test</h3>
        <p>Online Test :: (Search Engine Optimization (SEO)) Programming Test</p>
        <div class="digital-intro">
          <h5>Q.1  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod?</h5>
          <div class="radio">
            <input id="radio-1" name="radio" type="radio">
            <label for="radio-1" class="radio-label green-check">Lorem Ipsum is simply dummy</label>
          </div>
          <div class="radio">
            <input id="radio-2" name="radio" type="radio">
            <label  for="radio-2" class="radio-label green-check">Lorem Ipsum is simply dummy </label>
          </div>
          <div class="radio">
            <input id="radio-3" name="radio" type="radio">
            <label  for="radio-3" class="radio-label green-check">Lorem Ipsum is simply dummy</label>
          </div>
          <div class="radio">
            <input id="radio-4" name="radio" type="radio">
            <label  for="radio-4" class="radio-label green-check">Lorem Ipsum is simply dummy</label>
          </div>
          <div class=" prev-next">
            <a href="" title="Previous"> <button type="button" class="btn btn-primary float-left text-uppercase"> Previous</button></a>
            <a href="" title="Next"> <button type="button" class="btn btn-primary next-bt text-uppercase"> Next </button></a>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        <?php 
          include 'sidebar-right.php';
          ?>
      </div>
    </div>
  </div>
</section>
<!--     footer include --> 
<?php
  include 'footer.php';
  ?>
