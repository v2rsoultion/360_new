<!--  footer panel start -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="footer">
                    <a href="index.php" title="Digital Marketing">
                        <img src="images/footer-logo.png" alt="footer-logo" class="img-fluid">
                    </a>
                </div>
            </div>
            <ul>
                <li><a href="index.php" class="text-uppercase" title="Home">Home</a></li>
                <li><a href="videos-tutorials.php" class="text-uppercase" title="Video Tutorials">VIDEO TUTORIALS</a></li>
                <li> <a href="practice-quiz.php" class="text-uppercase" title="Quiz Tests">QUIZ TESTS</a></li>
                <li> <a href="" class="text-uppercase" title="Interview Questions">INTERVIEW QUESTIONS</a></li>
                <li> <a href="" class="text-uppercase" title="technical Terms">TECHNICAL TERMS</a></li>
            </ul>
        </div>
    </div>
</footer>
<section id="last-footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-12">
                <ul>
                    <li>Copyright &copy
                        <a href="" title="360DigitalGyan">360DigitalGyan</a>
                        All Rights Reserved
                    </li>
                </ul>
            </div>
            <div class="col-xl-6  col-md-6 col-12 text-right">
                <ul>
                    <li>Designed & Developed by
                        <a href="http://www.wscubetech.com/" target="blank" title="Affordable Webdesign Company, Website Development Company, Creative Webdesign Company, Website Services, Website Packages, Hosting" class="wstech">WsCube Tech</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--  footer panel close -->
</body>
<script src="js/jquery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script src="js/owl.carousel.min.js"></script>
<!--    <script src="js/aos.js"></script> -->
<script type="text/javascript" src="js/wow.min.js"></script>
<!-- Include Below JS After Your jQuery.min File -->
<script type="text/javascript" src="js/webslidemenu.js"></script>
<script src="js/bootstrapvalidator.min.js"></script>
<!-- <script src="js/jquery.tagsinput-revisited.js"></script> -->
<script src="js/stratum.min.js"></script>
<script type="text/javascript">
// main-slider start
$(document).ready(function() {
    var owl = $("#main-slider");
    owl.owlCarousel({
        navigation: true,
        autoplay: true,
        items: 1,
        loop: true,
        margin: 10,
        dots: false,
        nav: true,
        responsive: {
            0: {
                dots: true,
                nav: false,
                items: 1
            },
            600: {
                nav: false,
                dots: true,
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    var owl = $("#related-videos");
    owl.owlCarousel({
        navigation: true,
        autoplay: true,
        items: 4,
        loop: true,
        margin: 10,
        dots: false,
        nav: true,
        responsive: {
            0: {
                dots: false,
                nav: false,
                items: 2
            },
            600: {
                nav: false,
                dots: false,
                items: 3
            },
            1000: {
                nav: true,
                loop: true,
                dots: false,
                items: 4
            }
        }
    });

    // email marketing accordian
    $(".accordion_head").click(function() {
        if ($('.accordion_body').is(':visible')) {
            $(".accordion_body").slideUp(300);
            $(".plusminus").text('+');
        }
        if ($(this).next(".accordion_body").is(':visible')) {
            $(this).next(".accordion_body").slideUp(300);
            $(this).children(".plusminus").text('+');
        } else {
            $(this).next(".accordion_body").slideDown(300);
            $(this).children(".plusminus").text('-');
        }
    });
    // header fix function
    var id;
    if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
        id = "body";
    } else {
        id = window;
    }

    $(id).bind('scroll', function() {
        if ($(id).scrollTop() > 200) {
            $(".stepprocess1").addClass("stepprocess1_scroll");
            $(".scroollogo").addClass("scroollogo2");
            $(".menu-other").addClass("menu-padding");
        } else {
            $(".stepprocess1").removeClass("stepprocess1_scroll");
            $(".scroollogo").removeClass("scroollogo2");
            $(".menu-other").removeClass("menu-padding");
        }
    });

});
// fuction toggle for submenu
if ($(window).width() <= 900) {

    $(".flip").click(function() {
        $(this).next().slideToggle();
    });
}

// practice quiz function for check
$('.green-check').click(function() {
    $('.green-check i').remove();
    $(this).append('<i class="fas fa-check"></i>');
});

// animation 
wow = new WOW({
    animateClass: 'animated',
    offset: 100,
    callback: function(box) {
        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
    }
});
wow.init();
</script>
<script type="text/javascript">
$('.toggle').click(function() {
    $('#target').toggle('slow');
});

// courses tabbing
$("document").ready(function() {
    $(".tab-slider--body").hide();
    $(".tab-slider--body:first").show();
});

$(".tab-slider--nav li").click(function() {
    $(".tab-slider--body").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();
    if ($(this).attr("rel") == "tab2") {
        $('.tab-slider--tabs').addClass('slide');
    } else {
        $('.tab-slider--tabs').removeClass('slide');
    }
    $(".tab-slider--nav li").removeClass("active");
    $(this).addClass("active");
});

// lazy loader
// $(".loader .vedio-1").slice(3).hide();

//  var mincount = 1;
//  var maxcount = 3;


//  $(window).scroll(function () {
//      if ($(window).scrollTop() + $(window).height() >= $(document).height() - 500) {
//          $(".loader .student-names2").slice(mincount, maxcount).show();

//          mincount = mincount + 1;
//          maxcount = maxcount + 1

//      }
//  });


//  $(".loader2 .student-names2").slice(3).hide();

//  var mincount = 1;
//  var maxcount = 3;


//  $(window).scroll(function () {
//      if ($(window).scrollTop() + $(window).height() >= $(document).height() - 500) {
//          $(".loader .vedio-1").slice(mincount, maxcount).show();

//          mincount = mincount + 1;
//          maxcount = maxcount + 1

//      }
//  });
</script>
<!-- form validation  start-->
<script type="text/javascript">
$(document).ready(function() {
    $('#login-popup').bootstrapValidator({
        feedbackIcons: {

            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            pwd: {
                validators: {

                    notEmpty: {
                        message: 'Please enter your Password'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must contain at least 8 characters!'
                    }
                }
            },
            email: {
                validators: {
                    emailAddress: {
                        message: 'Please enter your email address'
                    },
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    }
                }
            },

        }
    })
    $('#signup-form').bootstrapValidator({
        feedbackIcons: {

            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your Full name'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The name can consist of alphabetical characters and spaces only'
                    }
                }
            },
            mobilenumber: {
                validators: {

                    stringLength: {
                        max: 10,
                        min: 10,
                        message: 'Please enter valid phone number'
                    }
                }
            },
            pwd: {
                validators: {

                    notEmpty: {
                        message: 'Please enter your password'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must contain at least 8 characters!'
                    }
                }
            },
            cnfrmpwd: {
                validators: {

                    notEmpty: {
                        message: 'Please enter your confirm password'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must contain at least 8 characters!'
                    }
                }
            },
            email: {
                validators: {
                    emailAddress: {
                        message: 'Please enter your email address'
                    },
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    }
                }
            },

        }
    })
    // for phone number validation
    $(function() {
        $('.numeric').on('input', function(event) {
            this.value = this.value.replace(/[^0-9]/g, '');
        });
    });
    $('#forgot-popup').bootstrapValidator({
        feedbackIcons: {

            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    emailAddress: {
                        message: 'Please enter your email address'
                    },
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    }
                }
            },

        }
    })
});
// password show and hide 
$(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

// finding spotting errors page
function hideQues(qs) {
    $('#ans-option' + qs).slideToggle('slow', function() {
        if ($('#ans-option' + qs).is(":visible")) {
            $("#reveal" + qs).html('Hide Answer');
            $("#show-answer" + qs).css('border-top', '1px solid rgb(231, 231, 231)');
        } else {
            $("#reveal" + qs).html('View Answer');
            $("#show-answer" + qs).css('border-top', '0px');
        }
    });
}

//  User login/Signup popup
$("#loagin_popup").click(function() {
    $('#login-popup').hide();
    $('#signup-popup').show();
});

$("#sign_popup").click(function() {
    $('#login-popup').show();
    $('#signup-popup').hide();
});

$(".close").click(function() {
    $('#login-popup').hide();
    $('#signup-popup').hide();
});

$(".loginShow").click(function() {
    $('#login-popup').show();
});

$(".singShow").click(function() {
    $('#signup-popup').show();
});
$("#forgotpassword").click(function() {
    $('#forgot-password').show();
});

$("#forgotpassword").click(function() {
    $('#login-popup').hide();
});
$(".closebtn").click(function() {
    $('#forgot-password').hide();
});

$("#forgot_login").click(function() {
    $('#forgot-password').hide();
});

$("#forgot_login").click(function() {
    $('#login-popup').show();
});

// tag autocomplete for post your question popup
$(function() {

    $('#form-tags-3').tagsInput({
        'unique': true,
        'minChars': 2,
        'maxChars': 10,
        'limit': 20,
        'validationPattern': new RegExp('^[a-zA-Z]+$')
    });


});
$(document).ready(function() {
    $('#key-form .all-button ul li a').click(function() {
        $('#key-form .all-button li a').removeClass("active");
        $(this).addClass("active");
    });
});

$(".scrool-top").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
});
</script>
</html>