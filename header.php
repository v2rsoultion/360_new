<!DOCTYPE html>
<html>

<head>
    <title> Digital Marketing &amp; SEO Quiz | Interview Questions and technical Terms</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" type="image/png" href="http://www.360digitalgyan.com/images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!--<link href="css/aos.css" rel="stylesheet">-->
    <!--Main Menu File-->
    <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="css/white-gry.css" />
    <link href="https://fonts.googleapis.com/css?family=Heebo:300,400,500,700,600&amp;subset=hebrew" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700,600"" rel=" stylesheet">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/jquery.tagsinput-revisited.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>

<body>
    <!-- Mobile Header -->
    <!-- Mobile Header -->
    <div class="wsmobileheader stepprocess1 clearfix">
        <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
        <span class="smllogo"><a href="index.php"><img src="images/logo.png" width="190" class="mobile-logo" alt="logo" /></a></span>
        <div class="wss">
            <a title="Login" class="loginShow scrool-top" data-target="#login-popup"> <i class="fas fa-sign-in-alt"></i></a>
            <a title="Register" class="singShow scrool-top" data-target="#signup-popup"> <i class="fas fa-user"></i></a>
        </div>
    </div>
    <div class="headerfull stepprocess1">
        <div class="wsmain clearfix">
            <!--  <div class="smllogo "><a href="index.php" title="360DigitalGyan"><img src="images/main-logo.png" alt="img_fullsize" class="scroollogo" /></a></div> -->
            <nav class="wsmenu">
                <ul class="wsmenu-list menu-other ">
                    <li>
                        <div class="smllogo ">
                            <a href="index.php" title="360DigitalGyan">
                                <img src="images/main-logo.png" alt="img_fullsize" class="scroollogo" />
                            </a>
                        </div>
                    </li>
                    <li aria-haspopup="true">
                        <a class="navtext active" title="Videos tutorials"><span>Videos tutorials <i class="fas fa-chevron-down"></i></span></a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-3 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <li><a href="#" title="Video Tutorial (English)"><i class="fas fa-chevron-right"></i>Video Tutorial (English) </a></li>
                                            <div class="clearfix"></div>
                                            <li><a href="#" title="Video Tutorial (English)"><i class="fas fa-chevron-right"></i>Video Tutorial (Hindi)</a></li>
                                            <li> <a href="#" title="Technical Treams"><i class="fas fa-chevron-right"></i>Technical Treams</a></li>
                                            <li><a href="#" title="Questions Answer"><i class="fas fa-chevron-right"></i>Questions Answer</a> </li>
                                            <li><a href="#" title="Key Trends & Concepts"><i class="fas fa-chevron-right"></i> Key Trends & Concepts</a> </li>
                                            <li><a href="#" title="Pirate Quiz"><i class="fas fa-chevron-right"></i>Pirate Quiz</a> </li>
                                            <li><a href="#" title="Study Material"><i class="fas fa-chevron-right"></i>Study Material</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-xl-8">
                                        <div class="row">
                                            <div class="col-lg-4 col-xl-3 col-md-12 padding-remove interview-li interview-li2">
                                                <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                                    Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                                </h4>
                                                <ul class="wstliststy06 clearfix  interview_inner">
                                                    <li><a href="email-marketing.php" title="Email Marketing"><i class="fas fa-chevron-right"></i>Email Marketing</a></li>
                                                    <li><a href="#" title="Facebook"><i class="fas fa-chevron-right"></i>Facebook </a></li>
                                                    <li><a href="#" title="Big"><i class="fas fa-chevron-right"></i>Big</a></li>
                                                    <li><a href="#" title="Twitter"><i class="fas fa-chevron-right"></i>Twitter</a> </li>
                                                    <li><a href="#" title="Pay Per Clicl(PPC)"><i class="fas fa-chevron-right"></i>Pay Per Clicl(PPC) </a></li>
                                                    <li><a href="#" title="Google Ranking Algorithm"><i class="fas fa-chevron-right"></i>Google Ranking Algorithm</a></li>
                                                    <li><a href="#" title="Its Updares & Key Points"><i class="fas fa-chevron-right"></i>Its Updares & Key Points</a></li>
                                                    <li><a href="#" title="Digital Marketing"><i class="fas fa-chevron-right"></i>Digital Marketing</a></li>
                                                    <li><a href="#" title="Search Engine Optimization (SEO)"><i class="fas fa-chevron-right"></i>Search Engine Optimization (SEO)</a></li>
                                                    <li><a href="#" title="Search Engine MArketing (SEM)"><i class="fas fa-chevron-right"></i>Search Engine MArketing (SEM)</a></li>
                                                    <li><a href="#" title="Socile Media Optimization (SMO)"><i class="fas fa-chevron-right"></i>Socile Media Optimization (SMO)</a></li>
                                                    <li><a href="#" title="Social Media Marketing (SMM)"><i class="fas fa-chevron-right"></i>Social Media Marketing (SMM)</a></li>
                                                    <li><a href="#" title="Google Analytics"><i class="fas fa-chevron-right"></i>Google Analytics</a></li>
                                                    <li><a href="#" title="Google Adwords"><i class="fas fa-chevron-right"></i>Google Adwords</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-4 col-xl-3 col-md-12 padding-remove interview-li interview-li2">
                                                <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                                    Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                                </h4>
                                                <ul class="wstliststy06 clearfix  interview_inner">
                                                    <li><a href="email-marketing.php" title="Email Marketing"><i class="fas fa-chevron-right"></i>Email Marketing</a></li>
                                                    <li><a href="#" title="Facebook"><i class="fas fa-chevron-right"></i>Facebook </a></li>
                                                    <li><a href="#" title="Big"><i class="fas fa-chevron-right"></i>Big</a></li>
                                                    <li><a href="#" title="Twitter"><i class="fas fa-chevron-right"></i>Twitter</a> </li>
                                                    <li><a href="#" title="Pay Per Clicl(PPC)"><i class="fas fa-chevron-right"></i>Pay Per Clicl(PPC) </a></li>
                                                    <li><a href="#" title="Google Ranking Algorithm"><i class="fas fa-chevron-right"></i>Google Ranking Algorithm</a></li>
                                                    <li><a href="#" title="Its Updares & Key Points"><i class="fas fa-chevron-right"></i>Its Updares & Key Points</a></li>
                                                    <li><a href="#" title="Digital Marketing"><i class="fas fa-chevron-right"></i>Digital Marketing</a></li>
                                                    <li><a href="#" title="Search Engine Optimization (SEO)"><i class="fas fa-chevron-right"></i>Search Engine Optimization (SEO)</a></li>
                                                    <li><a href="#" title="Search Engine MArketing (SEM)"><i class="fas fa-chevron-right"></i>Search Engine MArketing (SEM)</a></li>
                                                    <li><a href="#" title="Socile Media Optimization (SMO)"><i class="fas fa-chevron-right"></i>Socile Media Optimization (SMO)</a></li>
                                                    <li><a href="#" title="Social Media Marketing (SMM)"><i class="fas fa-chevron-right"></i>Social Media Marketing (SMM)</a></li>
                                                    <li><a href="#" title="Google Analytics"><i class="fas fa-chevron-right"></i>Google Analytics</a></li>
                                                    <li><a href="#" title="Google Adwords"><i class="fas fa-chevron-right"></i>Google Adwords</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-4 col-xl-3 col-md-12 padding-remove interview-li interview-li2">
                                                <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                                    Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                                </h4>
                                                <ul class="wstliststy06 clearfix  interview_inner">
                                                    <li><a href="email-marketing.php" title="Email Marketing"><i class="fas fa-chevron-right"></i>Email Marketing</a></li>
                                                    <li><a href="#" title="Facebook"><i class="fas fa-chevron-right"></i>Facebook </a></li>
                                                    <li><a href="#" title="Big"><i class="fas fa-chevron-right"></i>Big</a></li>
                                                    <li><a href="#" title="Twitter"><i class="fas fa-chevron-right"></i>Twitter</a> </li>
                                                    <li><a href="#" title="Pay Per Clicl(PPC)"><i class="fas fa-chevron-right"></i>Pay Per Clicl(PPC) </a></li>
                                                    <li><a href="#" title="Google Ranking Algorithm"><i class="fas fa-chevron-right"></i>Google Ranking Algorithm</a></li>
                                                    <li><a href="#" title="Its Updares & Key Points"><i class="fas fa-chevron-right"></i>Its Updares & Key Points</a></li>
                                                    <li><a href="#" title="Digital Marketing"><i class="fas fa-chevron-right"></i>Digital Marketing</a></li>
                                                    <li><a href="#" title="Search Engine Optimization (SEO)"><i class="fas fa-chevron-right"></i>Search Engine Optimization (SEO)</a></li>
                                                    <li><a href="#" title="Search Engine MArketing (SEM)"><i class="fas fa-chevron-right"></i>Search Engine MArketing (SEM)</a></li>
                                                    <li><a href="#" title="Socile Media Optimization (SMO)"><i class="fas fa-chevron-right"></i>Socile Media Optimization (SMO)</a></li>
                                                    <li><a href="#" title="Social Media Marketing (SMM)"><i class="fas fa-chevron-right"></i>Social Media Marketing (SMM)</a></li>
                                                    <li><a href="#" title="Google Analytics"><i class="fas fa-chevron-right"></i>Google Analytics</a></li>
                                                    <li><a href="#" title="Google Adwords"><i class="fas fa-chevron-right"></i>Google Adwords</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-4 col-xl-3 col-md-12 padding-remove interview-li padding-remove">
                                                <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                                    Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                                </h4>
                                                <ul class="wstliststy06 clearfix interview_inner">
                                                    <li><a href="#" title="Content Marketing"><i class="fas fa-chevron-right"></i> Content Marketing </a></li>
                                                    <li><a href="#" title="Facebook Ads Quiz"><i class="fas fa-chevron-right"></i>Facebook Ads Quiz</a></li>
                                                    <li><a href="#" title="Digital Marketing"><i class="fas fa-chevron-right"></i>Digital Marketing</a></li>
                                                    <li><a href="#" title="Search Engine Optimization (SEO)"><i class="fas fa-chevron-right"></i>Search Engine Optimization (SEO)</a> </li>
                                                    <li><a href="#" title="Search Engine MArketing (SEM)"><i class="fas fa-chevron-right"></i>Search Engine MArketing (SEM) </a></li>
                                                    <li><a href="#" title="Social Media Marketing and Optimization (SMM)"><i class="fas fa-chevron-right"></i>Social Media Marketing & Optimization (SMM)</a></li>
                                                    <li><a href="#" title="Google Analytics"><i class="fas fa-chevron-right"></i>Google Analytics</a></li>
                                                    <li><a href="#" title="Google Adwords"><i class="fas fa-chevron-right"></i>Google Adwords</a></li>
                                                    <li><a href="#" ctitle="Google Webmaster"><i class="fas fa-chevron-right"></i>Google Webmaster</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li aria-haspopup="true">
                        <a href="#" class="navtext" title="Quiz"> <span>Quiz <i class="fas fa-chevron-down"></i></span> </a>
                        <div class="wsmegamenu clearfix halfmenu halfmenu2">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-6 col-xl-6 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            <li><a href="#" title="Email Marketing"><i class="fas fa-chevron-right"></i>Email Marketing</a></li>
                                            <li><a href="#" title="Facebook"><i class="fas fa-chevron-right"></i>Facebook </a></li>
                                            <li><a href="#" title="Big"><i class="fas fa-chevron-right"></i>Big</a></li>
                                            <li><a href="#" title="Twitter"><i class="fas fa-chevron-right"></i>Twitter</a> </li>
                                            <li><a href="#" title="Pay Per Clicl(PPC)"><i class="fas fa-chevron-right"></i>Pay Per Clicl(PPC) </a></li>
                                            <li><a href="#" title="Google Ranking Algorithm"><i class="fas fa-chevron-right"></i>Google Ranking Algorithm</a></li>
                                            <li><a href="#" title="Its Updares & Key Points"><i class="fas fa-chevron-right"></i>Its Updares & Key Points</a></li>
                                            <li><a href="#" title="Digital Marketing"><i class="fas fa-chevron-right"></i>Digital Marketing</a></li>
                                            <li><a href="#" title="Search Engine Optimization (SEO)"><i class="fas fa-chevron-right"></i>Search Engine Optimization (SEO)</a></li>
                                            <li><a href="#" title="Search Engine MArketing (SEM)"><i class="fas fa-chevron-right"></i>Search Engine MArketing (SEM)</a></li>
                                            <li><a href="#" title="Socile Media Optimization (SMO)"><i class="fas fa-chevron-right"></i>Socile Media Optimization (SMO)</a></li>
                                            <li><a href="#" title="Social Media Marketing (SMM)"><i class="fas fa-chevron-right"></i>Social Media Marketing (SMM)</a></li>
                                            <li><a href="#" title="Google Analytics"><i class="fas fa-chevron-right"></i>Google Analytics</a></li>
                                            <li><a href="#" title="Google Adwords"><i class="fas fa-chevron-right"></i>Google Adwords</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-xl-6 col-md-12 interview-li">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 interview_inner clearfix">
                                            <li><a href="#" title="Content Marketing"><i class="fas fa-chevron-right"></i>Content Marketing </a></li>
                                            <li><a href="#" title="Facebook Ads Quiz"><i class="fas fa-chevron-right"></i>Facebook Ads Quiz</a></li>
                                            <li><a href="#" title="Digital Marketing"><i class="fas fa-chevron-right"></i>Digital Marketing</a></li>
                                            <li><a href="#" title="Search Engine Optimization (SEO)"><i class="fas fa-chevron-right"></i>Search Engine Optimization (SEO)</a> </li>
                                            <li><a href="#" title="Search Engine MArketing (SEM) "><i class="fas fa-chevron-right"></i>Search Engine MArketing (SEM) </a></li>
                                            <li><a href="#" title="Social Media Marketing and Optimization (SMM)"><i class="fas fa-chevron-right"></i>Social Media Marketing and Optimization (SMM)</a></li>
                                            <li><a href="#" title="Google Analytics"><i class="fas fa-chevron-right"></i>Google Analytics</a></li>
                                            <li><a href="#" title="Google Adwords"><i class="fas fa-chevron-right"></i>Google Adwords</a></li>
                                            <li><a href="#" ctitle="Google Webmaster"><i class="fas fa-chevron-right"></i>Google Webmaster</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li aria-haspopup="true"><a href="#" class="navtext" title="Interview questions"> <span> Interview q.</span></a> </li>
                    <li aria-haspopup="true"><a href="#" class="navtext" title="Technical Terms"><span>Technical Terms</span></a></li>
                    <li aria-haspopup="true"><a href="question-page.php" class="navtext" title="Question & answer (forum)"><span>Question & answer</span></a> </li>
                    <li aria-haspopup="true"><a href="#" class="navtext" title="Key Terms"> <span>Key Terms</span></a></li>
                    <li aria-haspopup="true">
                        <a href="#" class="navtext" title="News Feed"> <span>News Feed</span> </a>
                        <ul class="sub-menu sub-menu2">
                            <li><a href="#"><i class="fas fa-chevron-right"></i>Content Marketing</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i>Facebook Ads Quiz</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i>Digital Marketing</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i>Help Center</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i>Google Webmaster</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true">
                        <a href="" class="navtext" title="Study material"> <span>Study material</span> </a>
                    </li>
                    <li aria-haspopup="true" class="wsshopmyaccount d-none d-md-block"><a class="register scrool-top singShow" title="Register" data-controls-modal="#signup-popup" data-backdrop="static" data-keyboard="false">Register</a></li>
                    <li aria-haspopup="true" class="wsshopmyaccount d-none d-md-block" data-backdrop="static" data-keyboard="false"><a class="loginShow scrool-top" title=" Login">Log in</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- login popup -->
    <div class="modal centered-modal in login-modal" id="login-popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <!--   -->
                <button type="button" class="close" title="Close" data-dismiss="modal">&times;</button>
                <div class="clearfix"></div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-6 col-md-6">
                            <h1>Hello</h1>
                            <img src="images/border.png" alt="border">
                            <h5>Sign in to your account</h5>
                            <form id="login-form">
                                <div class="col-md-12 col-xl-12 padding-remove">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-fields" placeholder="Email Id" name="email">
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-12  padding-remove">
                                    <div class="form-group">
                                        <input id="password-field" type="password" placeholder="Password" class="form-control form-fields" name="pwd" value="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" minlength="8">
                                        <span toggle="#password-field" class="far fa-eye field-icon toggle-password"></span>
                                    </div>
                                </div>
                                <label class="remember float-left">Remember me
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                                <div class="forgot float-right">
                                    <h3 id="forgotpassword">Forgot password?</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-xs-12">
                                    <button type="submit" class="Submit-btn all-button" title="SIGN IN">SIGN IN</button>
                                </div>
                            </form>
                            <h6 class="text-center">or sign up with</h6>
                            <div class="social-icon">
                                <ul>
                                    <li> <a href="https://www.facebook.com/digitalgyan360/" class="facebook" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li> <a href="https://twitter.com/360digitalgyan" title="Twiiter" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li> <a href="https://plus.google.com/106421900076739124383" class="google-plus" title="Google Plus" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                                </ul>
                            </div>
                            <u class="text-center">Don’t have an account? <a id="loagin_popup" title="Create">Create</a></u>
                        </div>
                        <div class="col-xl-6 col-md-6 d-none d-md-block">
                            <img src="images/login-img.png" alt="login-img" class="img-fluid login-side">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--     forgot password popup -->
    <div class="modal  login-modal" id="forgot-password">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <!--   -->
                <button type="button" title="Close" class="close closebtn" data-dismiss="modal">&times;</button>
                <div class="clearfix"></div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-6 col-md-6">
                            <h1>Forgot Password</h1>
                            <img src="images/border.png" alt="border" class="boder-img">
                            <form id="forgot-popup">
                                <div class="col-md-12 col-xl-12 padding-remove">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-fields" placeholder="Email Id" name="email" required>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-xs-12 padding-remove">
                                    <button type="submit" class="Submit-btn all-button" title="SUBMIT">SUBMIT</button>
                                </div>
                            </form>
                            <u class="text-center">Continue to <a id="forgot_login" title="Login"> Login</a></u>
                        </div>
                        <div class="col-xl-6 col-md-6 d-none d-md-block">
                            <img src="images/login-img.png" alt="login-img" class="img-fluid login-side">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- create account popup -->
    <div class="modal  login-modal" id="signup-popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <!--   -->
                <button type="button" class="close" title="Close" data-dismiss="modal">&times;</button>
                <div class="clearfix"></div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-6 col-md-6">
                            <h1>Hello</h1>
                            <img src="images/border.png" alt="border">
                            <h5>Sign up to your account</h5>
                            <form id="signup-form">
                                <div class="col-xl-12 col-12 padding-remove">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-fields" placeholder="Name" name="name" id="name">
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-12 padding-remove">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-fields" placeholder="Email Id" name="email">
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-12 padding-remove">
                                    <div class="form-group">
                                        <input type="text" class="form-control numeric form-fields" placeholder="Mobile No" name="mobilenumber">
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-12  padding-remove">
                                    <div class="form-group">
                                        <input id="password-field" type="password" placeholder="Password" class="form-control form-fields" name="pwd" value="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" minlength="8">
                                    </div>
                                </div>
                                <div class="col-xl-12 col-12 padding-remove">
                                    <div class="form-group">
                                        <input type="password" class="form-control form-fields" placeholder="Confirm Password" name="cnfrmpwd" id="cnfrmpwd" required>
                                    </div>
                                </div>
                                <label class="remember float-left">I agree to the <a href="">Terms & Conditions</a> and <a href="">Privacy Policy.</a>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-xs-12">
                                    <button type="submit" class="Submit-btn all-button" title="SIGN UP">SIGN UP</button>
                                </div>
                            </form>
                            <h6 class="text-center">or sign up with</h6>
                            <div class="social-icon">
                                <ul>
                                    <li> <a href="https://www.facebook.com/digitalgyan360/" class="facebook" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li> <a href="https://twitter.com/360digitalgyan" title="Twiiter" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li> <a href="https://plus.google.com/106421900076739124383" class="google-plus" title="Google Plus" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                                </ul>
                            </div>
                            <u class="text-center">Already have an account?<a id="sign_popup" class="signs-pop hidemodel" title="Login">Login</a></u>
                        </div>
                        <div class="col-xl-6 col-md-6 d-none d-md-block">
                            <img src="images/login-img.png" alt="login-img" class="img-fluid login-side">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>