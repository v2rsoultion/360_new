<!-- header include -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Videos Tutorial</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="index.php" class="home-main">Home</a>  <i class="fas fa-chevron-right"></i> <u class="mater">   Videos</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="vedios-tutorial">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-12 col-xl-9">
        <div class="vedio-form">
          <form name="myform" method="GET" action="">
            <div class="input-group" id="seacr_panel">
              <div class="select-dropdown">
                <select class="form-control" id="withSelect">
                  <option>All</option>
                  <option>hindi</option>
                  <option>english</option>
                </select>
              </div>
              <input type="text" name="s" class="form-control vedio-search" value="" placeholder="Search...">
              <span class="input-group-btn">
              <button type="submit" title="Search" class="btn btn-default header-submit">SEARCH</button>
              </span>
            </div>
          </form>
        </div>
        <div id="tutorials">
          <div class="col-xl-12 col-md-12 col-12">
            <h1 class="text-center">Videos Tutorial</h1>
          </div>
          <div class="tab-slider--nav">
            <ul class="tab-slider--tabs">
              <li class="tab-slider--trigger active text-uppercase" title="HINDI (15)" rel="hindi">HIndi (15)</li>
              <li class="tab-slider--trigger text-uppercase" title="ENGLISH (8)" rel="english">English (8)</li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          <div class="tab-slider--container container padding-remove">
            <div id="hindi" class="tab-slider--body">
              <div class="loader">
                <div class="row english-tab">
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/seo-img.png" class="img-fluid" alt="seo-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/seo-img.png" class="img-fluid" alt="seo-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/seo-img.png" class="img-fluid" alt="seo-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                  <div class="col-xl-4 col-md-6 col-6">
                    <div class="vedio-1">
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                      <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                        <h5>Lorem ipsum dolor sit amet
                          consectetur elit.
                        </h5>
                      </a>
                      <span><i class="fas fa-eye"></i>300</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="english" class="tab-slider--body">
            <div class="loader">
              <div class="row english-tab">
                <div class="col-xl-4 col-md-6 col-6">
                  <div class="vedio-1">
                    <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                    <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                      <h5>Lorem ipsum dolor sit amet
                        consectetur elit.
                      </h5>
                    </a>
                    <span><i class="fas fa-eye"></i>300</span>
                  </div>
                </div>
                <div class="col-xl-4 col-md-6 col-6">
                  <div class="vedio-1">
                    <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/seo-img.png" class="img-fluid" alt="seo-img"></a>
                    <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                      <h5>Lorem ipsum dolor sit amet
                        consectetur elit.
                      </h5>
                    </a>
                    <span><i class="fas fa-eye"></i>300</span>
                  </div>
                </div>
                <div class="col-xl-4 col-md-6 col-6">
                  <div class="vedio-1">
                    <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                    <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                      <h5>Lorem ipsum dolor sit amet
                        consectetur elit.
                      </h5>
                    </a>
                    <span><i class="fas fa-eye"></i>300</span>
                  </div>
                </div>
                <div class="col-xl-4 col-md-6 col-6">
                  <div class="vedio-1">
                    <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit."> <img src="images/related-img.png" class="img-fluid" alt="related-img"></a>
                    <a href="view-vedio.php" title="Lorem ipsum dolor sit amet consectetur elit.">
                      <h5>Lorem ipsum dolor sit amet
                        consectetur elit.
                      </h5>
                    </a>
                    <span><i class="fas fa-eye"></i>300</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        <?php 
          include 'sidebar-right.php';
          ?>
      </div>
    </div>
  </div>
</section>
<!--     footer include --> 
<?php
  include 'footer.php';
  ?>