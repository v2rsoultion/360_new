<!-- header include -->
<?php
  include 'header.php';
  ?>
<section id="bredcum">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-6">
                <h4 class="text-capitalize">Email Marketing</h4>
            </div>
            <div class="col-xl-6 col-md-6 col-6">
                <span class="text-capitalize float-right"><a href="index.php" class="home-main">Home</a> <i class="fas fa-chevron-right"></i> <u class="mater"> Email Marketing</u></span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section id="email-marketing">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-12 col-md-5" id="email-sidepanel">
                <div class="email-panelin">
                    <img src="images/vd.png" alt="vd">
                    <span class="text-uppercase">Video TUTORIALS</span>
                </div>
                <div class="clearfix"></div>
                <div class="seo-1 float-left">
                    <a href=""><img src="images/seo-img.png" alt="seo-img" width="81px" height="77px;"></a>
                    <div class="clearfix"></div>
                </div>
                <div class="seo-des float-left">
                    <p><a href="">SEO tutorial for beginners | S</a></p>
                </div>
                <div class="clearfix"></div>
                <div class="seo-1 float-left">
                    <a href=""><img src="images/search-engine.jpg" alt="search-engine" width="81px" height="77px;"></a>
                    <div class="clearfix"></div>
                </div>
                <div class="seo-des float-left">
                    <p><a href="">Search Engine Working | Rankin</a></p>
                </div>
                <div class="clearfix"></div>
                <div class="seo-1 float-left">
                    <a href=""><img src="images/type-seo.jpg" alt="type-seo" width="81px" height="77px;"></a>
                    <div class="clearfix"></div>
                </div>
                <div class="seo-des float-left">
                    <p><a href="">Types of SEO | White hat SEO |</a></p>
                </div>
                <div class="clearfix"></div>
                <div class="email-panelin">
                    <img src="images/vd.png" alt="vd">
                    <span class="text-uppercase">quiz practice test</span>
                </div>
            </div>
            <div class="col-xl-9 col-md-7">
                <div class="email-question">
                    <div class="accordion_container">
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Is The meaning of Email Deliver Ability & Email Open Rates?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>These are two important email marketing metrics that should serve as the initial measure of your email marketing success. Email deliverability rate (or acceptance rate) is the success rate of getting an email delivered to a person's email address. To find out the deliver ability rate of your email marketing, you simply take the number of emails delivered and divide it by the number that were sent.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p>What Are Spam Reports?</p class="text-capitalize"><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>It means how many times your emails were marked as junk mail/spam by recipients.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">Explain Un Subscribes?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>It means how many people unsubscribed from your emails.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Is Inbox Rates?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>It means how many people unsubscribed from your emails.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Are Bounce Rates?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p> The percentage of emails that were rejected by recipients' mail servers.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">Which Metrics Sholuld i Track For Email Marketing?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p> Over time, you need to track your email marketing efforts alongside your larger goals, whether they're direct sales (like B2C retail companies), website traffic and conversions (B2B or B2C with long sales cycles) or referral business.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">Tell Me About Good Email Open Rates?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p> Similar to the best day/time to send an email, this question can also be a bit slippery to nail down. It's like comparing apples and oranges. Even if two people in the same industry sent the same email at the same time with the same content, they could have very different open rates due to factors like list quality and personal relationship.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">How Often Should Send Emails To Customers?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>The jury is not as undecided on this as they are on the best time and day to send emails. The optimal frequency to contact your customers is one to four times a month. This is the frequency we use for our customers, and our clients see great results with a twice-a-month schedule.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">How Much Emails Sholuld I Sent To My Customers?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Once a month is fine for keeping your name in front of someone. Two-three times a month leaves enough time in between emails so you don't become a nuisance. Obviously, four times a month gets you to a weekly consistency. Essentially, you can keep increasing your send frequency as much as you want, but be conscious of your open and unsubscribe rates. When your engagement starts to decline, you know you've taken it too far.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">Tell me About The Best Time & Day To Send Emails?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Early mornings and weekends … but also says those days/times are also the most likely to get you unsubscribes and bounces. There are many right answers to this question. If you're looking for a fairly safe bet, try Wednesday afternoons around 2 p.m. But every person, recipient and email list is different. Try different days and times and find what works best for your audience.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">Which Practices Are best For Email Subject Lines?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>You have only 3-4 seconds before someone decides whether or not they are going to open your email, so it's important to make an impact.. Best practices for subject lines are debatable, especially when trying to compare different industries and topics. However, there are some ballpark best practices for subject lines that you can feel confident about following. Fifty characters is generally a good, safe rule of thumb to follow. However, people have seen great success with subject lines over 70 characters and less than 49, so feel free to experiment. Other tactics that work well in subject lines include asking questions, numbered lists and personalization. The best email subject lines tend to be specific, short and compelling. Set expectations up front and let the email content do the rest.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">List The Laws Of Email Marketing?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>As I mentioned above, it's good to email people you personally know. However, some email lists might include people from bought lists or lead gen services (which you should generally avoid). We don't recommend buying lists because there are laws in place to protect people from receiving certain types of unsolicited communications.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">How To Figure About What To Write About?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>As I mentioned above, it's good to email people you personally know. However, some email lists might include people from bought lists or lead gen services (which you should generally avoid). We don't recommend buying lists because there are laws in place to protect people from receiving certain types of unsolicited communications.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">How To Grow Email Subscribers List?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Email marketing is only as successful as the email list.A large email list doesn't automatically equal success, though so make sure that you're growing your list with the right people. If they're not actively engaged with you (or interested in what you have to say), your email marketing efforts will be for naught.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">Describe The Point Of Email Marketing?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Email marketing is a way of reaching many people at once in order to encourage them to do business with you. However, it's important that email marketers keep their end goals in mind (sales, conversions, web traffic, etc.), and execute their strategy in a way that leads their subscribers to willingly complete a desired action. This approach builds audience trust through emails that are valuable and entertaining for the reader, thus increasing the sender's credibility and getting that sender closer to their desired goal.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Is CAN-SPAM Act?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>The CAN-SPAM Act spells out the rules for email marketers, whether their intentions are good or bad. If you're sending emails for commercial use, you have to do the following:</p>
                                <ul>
                                    <li> Include an unsubscribe method.</li>
                                    <li> Actually unsubscribe someone when they request it, and do it in a timely manner.</li>
                                    <li> If sending for commercial uses, you need to include a physical address.</li>
                                    <li> While not explicitly banned, sending to people who have not opted in is discouraged.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Is WRAP-UP?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>There are many variables that affect the success or failure of your email marketing program. While it's nice to understand some of the industry standards and best practices for email marketing, the most important takeaway is that everyone's stats will be different as a result of these variables.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">how to prevent Email Getting Into Spam?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <ul>
                                    <li>Check if your domain name has been blacklisted.</li>
                                    <li> If you are using html, then html should be well coded</li>
                                    <li> Avoid using spam filtering words, punctuation or capital letters message in your content/subject. </li>
                                    <li>Spam filters look to see how many messages you are sending at a time.</li>
                                    <li>Encourage your receiver to add you as friend or contact.</li>
                                    <li>Keep the length of your subject under 45 characters. </li>
                                    <li> Don’t use lots of colored fonts.</li>
                                    <li>Only use one exclamation point at a time! </li>
                                    <li>Avoid using the phrases "to unsubscribe" and "to be removed" in your message body.</li>
                                    <li>If you want to deliver an important message to multiple groups better use the ‘BCC’ function. Most, if not all, email providers’ spam filters penalize your domain or IP with a higher spam score (meaning there’s a higher possibility of your emails going to junk folder) if they see that you are sending emails to bad email accounts. A bad email account is an address that doesn’t exist, has been disabled or has a full inbox.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Is the Use Of Landing Pages?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Email marketing is itself a powerful tool, but using landing pages can be added advantage. These are the pages which you put on your website through which customers can link to from your email. Landing pages are an extensively detailed image of your email campaign with more info, more images and even a purchase option, to attract recipients in buying what you're selling.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">Why To Use Landing Pages?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Landing page is a detailed page with multiple links. It performs below roles:</p>
                                <ul>
                                    <li>It will add extra explanation beyond your email copy.</li>
                                    <li>It will showcase your products.</li>
                                    <li>It will give your recipients tip lists.</li>
                                    <li>It will draw the recipients to your Website.</li>
                                    <li>It will track the campaign traffic towards the landing page.</li>
                                    <li>It will analyze the success of a campaign.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">how to Organize Mailing List?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Organizing the email addresses that you have gathered is very important. To do this you should combine all the emails you collected in to one single mass Email List, so that you get all the subscribers in one single file. You can use MS-Excel to combine the list. After combining the list, you can export it in to a text or a CSCV file as needed by your Mailing Client. An Excel worksheet can be converted to a text file by using Save As command. Click on Microsoft Office Button and Click Save As. In the save as type box, select the text file format for the worksheet. For example, click Text (Tab delimited) or CSV (Comma delimited) and you should perform some sorting to avoid any invalid emails.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Do You Mean By Formatting Emails?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Formatting emails is very much needed to clean your list from bad, dead email addresses. For example, when you are using your email list, there might be many wrongly typed email addresses in your list, some typo errors, syntax errors and possible some bouncing email addresses. On an average, every year, 22.5% email addresses get deactivated, banned or deleted. It can be a bad news if you are one of the victim dealing with an old email list.</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Is Validation?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>Validation is very important to avoid being marked as a spammer or being blacklisted. If you are paying to top Email Marketing Clients like Mail Chimp, Aweber etc. then this part is not necessary. But if you have got millions of Email addresses or you are running some other Bulk Email Clients which are hosted on your server then you probably you need these steps before launching any email campaign. We will discuss about these in detail later in chapter “Service Providers”</p>
                            </div>
                        </div>
                        <div class="first-question">
                            <div class="accordion_head">
                                <p class="text-capitalize">What Is OPT-IN?</p><span class="plusminus">+</span>
                            </div>
                            <div class="accordion_body" style="display: none;">
                                <p>An opt-in policy needs a potential customer to self-select the services which they wish to subscribe to and how any information they provide may be used. It is also referred as permission-based marketing. As discussed in the previous chapter, there are proper ways to gather email addresses using a CTA. Cash rewards, coupons, and convenience attract many consumers, but often these benefits come at the cost of an undisclosed contract for use of personal information. Individuals can choose sharing their data, but they should be afforded transparency over how their data will be used and with whom it will be shared.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--     footer include -->
<?php
  include 'footer.php';
  ?>